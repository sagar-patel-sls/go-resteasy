COMMIT := $(shell git describe --all --long)
VERGO := $(shell go version|cut -d ' ' -f 3,4|tr ' ' _)

ifeq ($(strip $(TEST_PGURI)),)
TEST_PGURI = postgres://postgres:postgres@localhost/re_test
endif

PIE = -buildmode=pie
RFLAG =
ifneq ($(RACE),)
PIE =
RFLAG = -race
export GORACE = halt_on_error=1
endif

install:
	go install $(PIE) -ldflags "-X main.vercommit=$(COMMIT) \
	-X main.vergo=$(VERGO)" $(RFLAG) ./cmd/go-resteasy.api

lint:
	cd /tmp && go get -u github.com/golangci/golangci-lint/cmd/golangci-lint \
	&& cd $(CURDIR)
	golangci-lint run -D staticcheck
# staticcheck defaults are all,-ST1000,-ST1003,-ST1016,-ST1020,-ST1021,-ST1022
	cd /tmp && go get -u honnef.co/go/tools/cmd/staticcheck && cd $(CURDIR)
	staticcheck -checks all,-ST1000 -unused.whole-program ./...

init_db:
	cd /tmp && go get -u -tags postgres \
	github.com/golang-migrate/migrate/cmd/migrate && cd $(CURDIR)
	migrate -path . -database $(TEST_PGURI)?sslmode=disable drop
	migrate -path config/sql/migrate -database $(TEST_PGURI)?sslmode=disable up

# -count 1 is the idiomatic way to disable test caching in package list mode
test: install lint unit_test integration_test
unit_test:
	go test -count=1 -cover $(RFLAG) -cpu 1,4 -tags unit ./...
integration_test: init_db
	go test -count=1 -cover $(RFLAG) -cpu 1,4 -tags integration ./...
