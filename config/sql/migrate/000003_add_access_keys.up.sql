CREATE TABLE access_keys (
  key uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  user_id uuid NOT NULL REFERENCES users (id) ON DELETE CASCADE,
  expires_at timestamp without time zone NOT NULL
);

CREATE INDEX access_keys_user_expires_index ON access_keys(user_id, expires_at);
