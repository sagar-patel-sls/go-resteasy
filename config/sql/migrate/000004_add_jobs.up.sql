DROP TYPE IF EXISTS jobstatus;
CREATE TYPE jobstatus AS ENUM ('ready', 'running', 'failed', 'complete', 'deleted');

CREATE TABLE jobs (
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  user_id uuid NOT NULL REFERENCES users (id) ON DELETE CASCADE,
  status jobstatus NOT NULL DEFAULT 'ready',
  started_at timestamp without time zone,
  completed_at timestamp without time zone
);

CREATE INDEX jobs_user_index ON jobs(user_id);
CREATE INDEX jobs_status_index ON jobs(status);
