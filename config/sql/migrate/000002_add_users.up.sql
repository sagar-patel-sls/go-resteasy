DROP TYPE IF EXISTS userrole;
CREATE TYPE userrole AS ENUM ('user', 'admin');

DROP TYPE IF EXISTS userstatus;
CREATE TYPE userstatus AS ENUM ('deleted', 'disabled', 'active');

CREATE TABLE users (
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  role userrole NOT NULL,
  first_name varchar(80) NOT NULL,
  last_name varchar(80) NOT NULL,
  email varchar(80) NOT NULL UNIQUE,
  status userstatus NOT NULL DEFAULT 'active',
  login varchar(40) NOT NULL UNIQUE,
  bcrypt_hash varchar(80) NOT NULL,
  phone_num varchar(80),
  address varchar(80),
  city varchar(80),
  state varchar(80),
  zip varchar(20)
);

CREATE UNIQUE INDEX users_login_index ON users(login);
CREATE INDEX users_status_index ON users(status);
