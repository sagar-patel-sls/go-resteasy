# go-resteasy

### REST API boilerplate in Go with no external dependencies

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/bconway/go-resteasy)](https://goreportcard.com/report/gitlab.com/bconway/go-resteasy)
[![pipeline status](https://gitlab.com/bconway/go-resteasy/badges/master/pipeline.svg)](https://gitlab.com/bconway/go-resteasy/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/bconway/go-resteasy?status.svg)](https://godoc.org/gitlab.com/bconway/go-resteasy)

Go-resteasy is a JSON- and XML-driven REST API boilerplate, written in Go (Golang), with no external dependencies (beyond a sample data store - PostgreSQL). It can be used to quickly build backend services with a REST API as its front. Go-resteasy was written from scratch.

## Features

- Routing
- Controllers with complete resource lifecycles (POST/GET/PUT/PATCH/DELETE)
- Extensible middleware for authentication, logging, and more
- Models and a Store (using a PostgreSQL database)
- JSON- and XML-driven ResponseWriters
- HATEOAS links on resources
- API Key-based authentication with users, roles, and resource ownership
- Logging, including request performance
- Configuration parser
- Comprehensive integration tests using Ruby minitest, see [integration/README.md](./integration/README.md)
- Binaries versioned by Git tag/commit and Go version

## Getting Started

```
docker-compose up
make test
```

## Usage

All configuration is available as environmental variables, with defaults defined in `internal/go-resteasy.api/config/config.go`.

```
psql -f config/sql/seed/seed_admin.sql -h localhost re_test postgres && \
RE_PGURI=postgres://postgres:postgres@localhost/re_test go-resteasy.api
```

## API

API controllers and their usage are outlined in `internal/go-resteasy.api/server/server.go` and their respective controller code. Seed data includes an 'admin' user with password 'hello123', which can be used to retrieve an API access key. For example, using `curl`:

```
$ curl -s -u admin:hello123 -H "Accept: application/json" \
http://localhost:8080/auth|python -m json.tool
{
  "accessKeys":[
    {
      "expiresAt":"2019-06-24T22:09:39.112095Z",
      "key":"b84547a1-2e7c-48a0-bd7c-fd03805788f9"
    }
  ],
  "email":"admin@example.com",
  "firstName":"Admin",
  "id":"17aedc60-ccb4-450b-83cc-ddaad45d1f3e",
  "lastName":"User",
  "links":[
    {
      "href":"/users/17aedc60-ccb4-450b-83cc-ddaad45d1f3e",
      "rel":"self"
    }
  ],
  "login":"admin",
  "role":"admin",
  "status":"active",
  "zip":"78701"
}
```

Need XML? **Not a problem:**

```
$ curl -s -u admin:hello123 -H "Accept: application/xml" \
http://localhost:8080/auth|xmllint --format -
<?xml version="1.0"?>
<Response>
  <User>
    <ID>17aedc60-ccb4-450b-83cc-ddaad45d1f3e</ID>
    <Role>admin</Role>
    <FirstName>Admin</FirstName>
    <LastName>User</LastName>
    <Email>admin@example.com</Email>
    <Status>active</Status>
    <Login>admin</Login>
    <Aip>78701</Aip>
    <AccessKeys>
      <AccessKey>
        <Key>b84547a1-2e7c-48a0-bd7c-fd03805788f9</Key>
        <ExpiresAt>2019-06-24T22:09:39.112095-05:00</ExpiresAt>
      </AccessKey>
    </AccessKeys>
    <Links>
      <Link rel="self" href="/users/17aedc60-ccb4-450b-83cc-ddaad45d1f3e"/>
    </Links>
  </User>
</Response>
```

To create a new job:

```
$ curl -s -H "Authorization: Bearer b84547a1-2e7c-48a0-bd7c-fd03805788f9" \
-H "Accept: application/json" -H "Content-Type: application/json" -X POST \
http://localhost:8080/jobs -d \
'{ "startedAt": "2015-01-01T19:51:41Z", "completedAt": "2017-01-01T19:51:41Z"}'|\
python -m json.tool
{
  "completedAt":"2017-01-01T19:51:41Z",
  "id":"46cac275-94dd-4be5-9535-9dad07ba2a8e",
  "links":[
    {
      "href":"/jobs/46cac275-94dd-4be5-9535-9dad07ba2a8e",
      "rel":"self"
    }
  ],
  "startedAt":"2015-01-01T19:51:41Z",
  "status":"ready",
  "userId":"17aedc60-ccb4-450b-83cc-ddaad45d1f3e"
}
```
