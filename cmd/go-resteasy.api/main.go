package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/config"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/server"
)

var (
	vercommit string // Git commit
	vergo     string // Go version
)

func main() {
	ver := flag.Bool("V", false, "Display the version number and exit.")
	flag.Parse()

	if *ver {
		fmt.Printf("go-resteasy.api commit: %s\nBuilt with: %s\n", vercommit,
			vergo)
		os.Exit(0)
	}

	cfg := config.New()
	srv := server.New(cfg)
	log.Print(fmt.Sprintf("Listening on %v", srv.Addr))
	log.Fatal(srv.ListenAndServe())
}
