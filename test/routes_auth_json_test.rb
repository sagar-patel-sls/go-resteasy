require 'minitest/autorun'
require_relative 'helpers'

class TestGetRootPathJson < Minitest::Test
  def setup
    @code, @headers, @body = get_request_json('/', ADMIN_KEY_JSON)
  end

  def test_root_not_found
    assert_equal 404, @code
    assert_equal 'application/json', @headers['content-type'][0]
    assert_equal 'not found', @body['error']
  end
end

class TestGetNotMatchingPathJson < Minitest::Test
  def setup
    @code, @headers, @body = get_request_json('/notmatching/', ADMIN_KEY_JSON)
  end

  def test_root_not_found
    assert_equal 404, @code
    assert_equal 'application/json', @headers['content-type'][0]
    assert_equal 'not found', @body['error']
  end
end

class TestAuthSuccessJson < Minitest::Test
  def setup
    @code, @headers, @body = auth_request_json(RE_USER, RE_PASS)
  end

  def test_success
    assert_equal 200, @code
    assert_equal 'application/json', @headers['content-type'][0]
    refute_nil @body['accessKeys'][0]['key']
  end
end

class TestBadAuthJson < Minitest::Test
  def setup
    @code, @headers, @body = auth_request_json(RE_USER, 'bad_pass')
  end

  def test_unauthorized
    assert_equal 401, @code
    assert_equal 'application/json', @headers['content-type'][0]
    assert_equal 'unauthorized', @body['error']
  end
end

class TestBadKeyJson < Minitest::Test
  def setup
    @code, @headers, @body = get_request_json('/users', ADMIN_KEY_JSON[0..-5])
  end

  def test_unauthorized_key
    assert_equal 401, @code
    assert_equal 'application/json', @headers['content-type'][0]
    assert_equal 'unauthorized', @body['error']
  end
end
