# Integration tests for go-resteasy

## Pre-requisites

- `gem install --user-install xml-simple`
- An instance of go-resteasy running, connected to a seeded database.

## Usage

`RE_URL=http://localhost:8080 RE_USER=admin RE_PASS=hello123 rake`

Or to run a single test file:

`RE_URL=http://localhost:8080 RE_USER=admin RE_PASS=hello123 ruby routes_auth_json_test.rb`
