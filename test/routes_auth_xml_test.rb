require 'minitest/autorun'
require_relative 'helpers'

class TestGetRootPathXml < Minitest::Test
  def setup
    @code, @headers, @body = get_request_xml('/', ADMIN_KEY_XML)
  end

  def test_root_not_found
    assert_equal 404, @code
    assert_equal 'application/xml', @headers['content-type'][0]
    assert_equal ['not found'], @body['Error']
  end
end

class TestGetNotMatchingPathXml < Minitest::Test
  def setup
    @code, @headers, @body = get_request_xml('/notmatching/', ADMIN_KEY_XML)
  end

  def test_root_not_found
    assert_equal 404, @code
    assert_equal 'application/xml', @headers['content-type'][0]
    assert_equal ['not found'], @body['Error']
  end
end

class TestAuthSuccessXml < Minitest::Test
  def setup
    @code, @headers, @body = auth_request_xml(RE_USER, RE_PASS)
  end

  def test_success
    assert_equal 200, @code
    assert_equal 'application/xml', @headers['content-type'][0]
    refute_nil @body['User'][0]['AccessKeys'][0]['AccessKey'][0]['Key'][0]
  end
end

class TestBadAuthXml < Minitest::Test
  def setup
    @code, @headers, @body = auth_request_xml(RE_USER, 'bad_pass')
  end

  def test_unauthorized
    assert_equal 401, @code
    assert_equal 'application/xml', @headers['content-type'][0]
    assert_equal ['unauthorized'], @body['Error']
  end
end

class TestBadKeyXml < Minitest::Test
  def setup
    @code, @headers, @body = get_request_xml('/users', ADMIN_KEY_XML[0..-5])
  end

  def test_unauthorized_key
    assert_equal 401, @code
    assert_equal 'application/xml', @headers['content-type'][0]
    assert_equal ['unauthorized'], @body['Error']
  end
end
