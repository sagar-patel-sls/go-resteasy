require 'securerandom'
require 'time'
require 'net/http'
require 'json'
require 'xmlsimple'

RE_URL = ENV['RE_URL']
RE_USER = ENV['RE_USER']
RE_PASS = ENV['RE_PASS']

def default_user_json
  { :role => 'user', :firstName => SecureRandom.hex(4), :lastName => SecureRandom.hex(4), :email => "#{SecureRandom.uuid}@example.com", :login => "#{SecureRandom.uuid}", :password => "#{SecureRandom.uuid}", :address => SecureRandom.hex(10), :zip => "#{rand(10000..99999)}" }
end

def default_user_xml
  { :Role => ['user'], :FirstName =>[SecureRandom.hex(4)], :LastName => [SecureRandom.hex(4)], :Email =>["#{SecureRandom.uuid}@example.com"], :Login => ["#{SecureRandom.uuid}"], :Password => ["#{SecureRandom.uuid}"], :Address => [SecureRandom.hex(10)], :Zip =>["#{rand(10000..99999)}"] }
end

def default_job_json
  { :startedAt => Time.now.utc.iso8601, :completedAt => Time.new(2050).utc.iso8601 }
end

def default_job_xml
  { :StartedAt => [Time.now.utc.iso8601], :CompletedAt => [Time.new(2050).utc.iso8601] }
end

def auth_request_json(login, pass)
  uri = URI("#{RE_URL}/auth")
  req = Net::HTTP::Get.new(uri, { 'Accept' =>  'application/json' })
  req.basic_auth(login, pass)
  res = Net::HTTP.start(uri.hostname, uri.port) do |http|
    http.request(req)
  end

  code = res.code.to_i
  headers = res.to_hash
  body = JSON[res.body]
  return code, headers, body
end

def user_key_id_json(login, pass)
  code, headers, body = auth_request_json(login, pass)
  return body['accessKeys'][0]['key'], body['id']
end

ADMIN_KEY_JSON, ADMIN_ID_JSON = user_key_id_json(RE_USER, RE_PASS)

def request_json(method, suffix, key)
  uri = URI("#{RE_URL}#{suffix}")
  req_headers = { 'Accept' =>  'application/json', 'Authorization' => "Bearer #{key}" }

  case method
  when :get
    req = Net::HTTP::Get.new(uri, req_headers)
  when :delete
    req = Net::HTTP::Delete.new(uri, req_headers)
  end

  res = Net::HTTP.start(uri.hostname, uri.port) do |http|
    http.request(req)
  end

  code = res.code.to_i
  headers = res.to_hash
  # DELETE will return a nil body
  body = res.body.nil? ? res.body : JSON[res.body]
  return code, headers, body
end

def get_request_json(suffix, key)
  request_json(:get, suffix, key)
end

def del_request_json(suffix, key)
  request_json(:delete, suffix, key)
end

def payl_request_json(method, suffix, key, payl, raw_json = false)
  uri = URI("#{RE_URL}#{suffix}")
  req_headers = { 'Accept' =>  'application/json', 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{key}" }

  case method
  when :post
    req = Net::HTTP::Post.new(uri, req_headers)
  when :put
    req = Net::HTTP::Put.new(uri, req_headers)
  when :patch
    req = Net::HTTP::Patch.new(uri, req_headers)
  else
    puts "Unknown/unsupported method: #{method}"
    exit
  end
  req.body = raw_json ? payl : payl.to_json

  res = Net::HTTP.start(uri.hostname, uri.port) do |http|
    http.request(req)
  end

  code = res.code.to_i
  headers = res.to_hash
  body = JSON[res.body]
  return code, headers, body
end

def auth_request_xml(login, pass)
  uri = URI("#{RE_URL}/auth")
  req = Net::HTTP::Get.new(uri, { 'Accept' =>  'application/xml' })
  req.basic_auth(login, pass)
  res = Net::HTTP.start(uri.hostname, uri.port) do |http|
    http.request(req)
  end

  code = res.code.to_i
  headers = res.to_hash
  body = XmlSimple.xml_in(res.body)
  return code, headers, body
end

def user_key_id_xml(login, pass)
  code, headers, body = auth_request_xml(login, pass)
  return body['User'][0]['AccessKeys'][0]['AccessKey'][0]['Key'][0], body['User'][0]['ID'][0]
end

ADMIN_KEY_XML, ADMIN_ID_XML = user_key_id_xml(RE_USER, RE_PASS)

def request_xml(method, suffix, key)
  uri = URI("#{RE_URL}#{suffix}")
  req_headers = { 'Accept' =>  'application/xml', 'Authorization' => "Bearer #{key}" }

  case method
  when :get
    req = Net::HTTP::Get.new(uri, req_headers)
  when :delete
    req = Net::HTTP::Delete.new(uri, req_headers)
  end

  res = Net::HTTP.start(uri.hostname, uri.port) do |http|
    http.request(req)
  end

  code = res.code.to_i
  headers = res.to_hash
  # DELETE will return a nil body
  body = res.body.nil? ? res.body : XmlSimple.xml_in(res.body)
  return code, headers, body
end

def get_request_xml(suffix, key)
  request_xml(:get, suffix, key)
end

def del_request_xml(suffix, key)
  request_xml(:delete, suffix, key)
end

def payl_request_xml(method, suffix, key, payl, xml_root, raw_xml = false)
  uri = URI("#{RE_URL}#{suffix}")
  req_headers = { 'Accept' =>  'application/xml', 'Content-Type' => 'application/xml', 'Authorization' => "Bearer #{key}" }

  case method
  when :post
    req = Net::HTTP::Post.new(uri, req_headers)
  when :put
    req = Net::HTTP::Put.new(uri, req_headers)
  when :patch
    req = Net::HTTP::Patch.new(uri, req_headers)
  else
    puts "Unknown/unsupported method: #{method}"
    exit
  end
  req.body = raw_xml ? payl : XmlSimple.xml_out(payl, 'RootName' => xml_root)

  res = Net::HTTP.start(uri.hostname, uri.port) do |http|
    http.request(req)
  end

  code = res.code.to_i
  headers = res.to_hash
  body = XmlSimple.xml_in(res.body)
  return code, headers, body
end
