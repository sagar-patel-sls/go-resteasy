require 'minitest/autorun'
require_relative 'helpers'

class TestCreateUserXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @code, @headers, @body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')
  end

  def test_user_created
    assert_equal 201, @code
    assert_equal 'application/xml', @headers['content-type'][0]
    refute_nil @body['User'][0]['ID'][0]
    assert_equal @user_payl[:Role][0], @body['User'][0]['Role'][0]
    assert_equal @user_payl[:Email][0], @body['User'][0]['Email'][0]
    assert_equal ['active'], @body['User'][0]['Status']
    assert_equal "/users/#{@body['User'][0]['ID'][0]}", @body['User'][0]['Links'][0]['Link'][0]['href']
  end
end

class TestCreateUserSubtreeXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @code, @headers, @body = payl_request_xml(:post, '/users/', ADMIN_KEY_XML, @user_payl, 'User')
  end

  def test_user_created_subtree
    assert_equal 201, @code
    assert_equal 'application/xml', @headers['content-type'][0]
    refute_nil @body['User'][0]['ID'][0]
    assert_equal @user_payl[:Role][0], @body['User'][0]['Role'][0]
    assert_equal @user_payl[:Email][0], @body['User'][0]['Email'][0]
    assert_equal ['active'], @body['User'][0]['Status']
    assert_equal "/users/#{@body['User'][0]['ID'][0]}", @body['User'][0]['Links'][0]['Link'][0]['href']
  end
end

class TestCreateUserBadPayloadXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @code, @headers, @body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, XmlSimple.xml_out(@user_payl, 'RootName' => 'User')[0..-5], 'User', true)
  end

  def test_incomplete_xml
    assert_equal 400, @code
    assert_equal ['XML syntax error on line 10: unexpected EOF'], @body['Error']
  end
end

class TestCreateUserMissingFieldsXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @user_payl.delete(:Email)
    @code, @headers, @body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')
  end

  def test_missing_Email
    assert_equal 400, @code
    assert_equal ['required field missing: email'], @body['Error']
  end
end

class TestCreateAndGetUserXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @getcode, @getheaders, @getbody = get_request_xml("/users/#{@postbody['User'][0]['ID'][0]}", ADMIN_KEY_XML)
  end

  def test_user_created_and_retrieved
    assert_equal 200, @getcode
    refute_nil @getbody['User'][0]['ID'][0]
    assert_equal @user_payl[:Role][0], @getbody['User'][0]['Role'][0]
    assert_equal @user_payl[:Email][0], @getbody['User'][0]['Email'][0]
    assert_equal ['active'], @getbody['User'][0]['Status']
    assert_equal "/users/#{@getbody['User'][0]['ID'][0]}", @getbody['User'][0]['Links'][0]['Link'][0]['href']
  end
end

class TestCreateAndGetAllUsersXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @post1code, @post1headers, @post1body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @user_payl = default_user_xml
    @post2code, @post2headers, @post2body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @user_payl = default_user_xml
    @post3code, @post3headers, @post3body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    # Verify all 3 users show in the /users list after creation
    @ids = []
    @allcode, @allheaders, @allbody = get_request_xml("/users", ADMIN_KEY_XML)
    @allbody['Users'][0]['User'].each do |user|
      @ids << user['ID'][0]
    end
  end

  def test_user_all_created
    assert_equal 200, @allcode
    assert @ids.include?(@post1body['User'][0]['ID'][0])
    assert @ids.include?(@post2body['User'][0]['ID'][0])
    assert @ids.include?(@post3body['User'][0]['ID'][0])
  end
end

class TestCreateAndUpdateUserXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @user_upd = @user_payl
    @user_upd[:Email] = ["#{SecureRandom.uuid}@example.com"]
    @user_upd[:Status] = [@postbody['User'][0]['Status'][0]]
    @putcode, @putheaders, @putbody = payl_request_xml(:put, "/users/#{@postbody['User'][0]['ID'][0]}", ADMIN_KEY_XML, @user_upd, 'User')
  end

  def test_user_created_and_updated
    assert_equal 200, @putcode
    assert_equal @postbody['User'][0]['ID'][0], @putbody['User'][0]['ID'][0]
    refute_equal @postbody['User'][0]['Email'][0], @putbody['User'][0]['Email'][0]
    assert_equal @user_upd[:Email][0], @putbody['User'][0]['Email'][0]
  end
end

class TestCreateAndPatchUserXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @user_patch = { :Email => ["#{SecureRandom.uuid}@example.com"] }
    @patchcode, @patchheaders, @patchbody = payl_request_xml(:patch, "/users/#{@postbody['User'][0]['ID'][0]}", ADMIN_KEY_XML, @user_patch, 'User')
  end

  def test_user_created_and_patched
    assert_equal 200, @patchcode
    assert_equal @postbody['User'][0]['ID'][0], @patchbody['User'][0]['ID'][0]
    refute_equal @postbody['User'][0]['Email'][0], @patchbody['User'][0]['Email'][0]
    assert_equal @user_patch[:Email][0], @patchbody['User'][0]['Email'][0]
  end
end

class TestCreateAndDeleteUserXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @delcode, @delheaders, @delbody = del_request_xml("/users/#{@postbody['User'][0]['ID'][0]}", ADMIN_KEY_XML)

    @getcode, @getheaders, @getbody = get_request_xml("/users/#{@postbody['User'][0]['ID'][0]}", ADMIN_KEY_XML)

    # Verify the user no longer shows in the /users list after deletion
    @ids = []
    @allcode, @allheaders, @allbody = get_request_xml("/users", ADMIN_KEY_XML)
    @allbody['Users'][0]['User'].each do |user|
      @ids << user['ID'][0]
    end
  end

  def test_user_created_and_deleted
    assert_equal 204, @delcode
    assert_nil @delbody

    assert_equal 200, @getcode
    assert_equal ['deleted'], @getbody['User'][0]['Status']

    assert_equal 200, @allcode
    refute @ids.include?(@postbody['User'][0]['ID'][0])
  end
end

class TestGetUserBadFormatIdXml < Minitest::Test
  def setup
    @code, @headers, @body = get_request_xml('/users/not-a-uuid', ADMIN_KEY_XML)
  end

  def test_parse_id_failure
    assert_equal 400, @code
    assert_equal ['ERROR: invalid input syntax for type uuid: "not-a-uuid" (SQLSTATE 22P02)'], @body['Error']
  end
end

class TestGetUserIdNotFoundXml < Minitest::Test
  def setup
    @code, @headers, @body = get_request_xml('/users/00000000-0000-9000-0000-000000000000', ADMIN_KEY_XML)
  end

  def test_id_not_found
    assert_equal 404, @code
    assert_equal ['not found'], @body['Error']
  end
end

class TestUserCreateUserXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @code, @headers, @body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    user_key, user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])
    @uu_payl = default_user_xml
    @usercode, @userheaders, @userbody = payl_request_xml(:post, '/users', user_key, @uu_payl, 'User')
  end

  def test_user_forbidden
    assert_equal 403, @usercode
    assert_equal ['forbidden'], @userbody['Error']
  end
end

class TestUserGetUsersXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    user_key, user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])
    @getcode, @getheaders, @getbody = get_request_xml('/users', user_key)
  end

  def test_user_single_user
    assert_equal 200, @getcode
    assert_equal 1, @getbody['Users'][0]['User'].length
    assert_equal @postbody['User'][0]['ID'], @getbody['Users'][0]['User'][0]['ID']
  end
end

class TestUserDelUserXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    user_key, user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])
    @delcode, @delheaders, @delbody = del_request_xml("/users/#{@postbody['User'][0]['ID'][0]}", user_key)
  end

  def test_user_fail_del_self
    assert_equal 403, @delcode
    assert_equal ['forbidden'], @delbody['Error']
  end
end

class TestUserGetDifferentUserXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @user1code, @user1headers, @user1body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')
    user1_key, user1_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])

    @user_payl = default_user_xml
    @user2code, @user2headers, @user2body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @getcode, @getheaders, @getbody = get_request_xml("/users/#{@user2body['User'][0]['ID'][0]}", user1_key)
  end

  def test_user_fail_get_different_user
    assert_equal 404, @getcode
    assert_equal ['not found'], @getbody['Error']
  end
end

class TestUserUpdateDifferentUserXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @user1code, @user1headers, @user1body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')
    user1_key, user1_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])

    @user_payl = default_user_xml
    @user2code, @user2headers, @user2body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @user_upd = @user_payl
    @user_upd[:Email] = ["#{SecureRandom.uuid}@example.com"]
    @putcode, @putheaders, @putbody = payl_request_xml(:put, "/users/#{@user2body['User'][0]['ID'][0]}", user1_key, @user_upd, 'User')
  end

  def test_user_fail_update_different_user
    assert_equal 404, @putcode
    assert_equal ['not found'], @putbody['Error']
  end
end

class TestUserPatchDifferentUserXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @user1code, @user1headers, @user1body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')
    user1_key, user1_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])

    @user_payl = default_user_xml
    @user2code, @user2headers, @user2body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @user_patch = { :Email => ["#{SecureRandom.uuid}@example.com"] }
    @patchcode, @patchheaders, @patchbody = payl_request_xml(:patch, "/users/#{@user2body['User'][0]['ID'][0]}", user1_key, @user_patch, 'User')
  end

  def test_user_fail_patch_different_user
    assert_equal 404, @patchcode
    assert_equal ['not found'], @patchbody['Error']
  end
end

class TestAdminChangeUserTypePutXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @user_upd = @user_payl
    @user_upd[:Role] = ['admin']
    @user_upd[:Status] = [@postbody['User'][0]['Status'][0]]
    @putcode, @putheaders, @putbody = payl_request_xml(:put, "/users/#{@postbody['User'][0]['ID'][0]}", ADMIN_KEY_XML, @user_upd, 'User')
  end

  def test_admin_change_Role_patch
    assert_equal 200, @putcode
    assert_equal @postbody['User'][0]['ID'], @putbody['User'][0]['ID']
    refute_equal @postbody['User'][0]['Role'], @putbody['User'][0]['Role']
    assert_equal @user_upd[:Role], @putbody['User'][0]['Role']
  end
end

class TestUserChangeUserTypePutXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')
    user_key, user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])

    @user_upd = @user_payl
    @user_upd[:Role] = ['admin']
    @user_upd[:Status] = [@postbody['User'][0]['Status'][0]]
    @putcode, @putheaders, @putbody = payl_request_xml(:put, "/users/#{@postbody['User'][0]['ID'][0]}", user_key, @user_upd, 'User')
  end

  def test_user_change_Role_patch
    assert_equal 200, @putcode
    assert_equal @postbody['User'][0]['ID'], @putbody['User'][0]['ID']
    assert_equal @postbody['User'][0]['Role'], @putbody['User'][0]['Role']
  end
end

class TestAdminChangeUserTypePatchXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @user_patch = { :Role => ['admin'] }
    @patchcode, @patchheaders, @patchbody = payl_request_xml(:patch, "/users/#{@postbody['User'][0]['ID'][0]}", ADMIN_KEY_XML, @user_patch, 'User')
  end

  def test_admin_change_Role_patch
    assert_equal 200, @patchcode
    assert_equal @postbody['User'][0]['ID'], @patchbody['User'][0]['ID']
    refute_equal @postbody['User'][0]['Role'], @patchbody['User'][0]['Role']
    assert_equal @user_patch[:Role], @patchbody['User'][0]['Role']
  end
end

class TestUserChangeUserTypePatchXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')
    user_key, user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])

    @user_patch = { :Role => ['admin'] }
    @patchcode, @patchheaders, @patchbody = payl_request_xml(:patch, "/users/#{@postbody['User'][0]['ID'][0]}", user_key, @user_patch, 'User')
  end

  def test_user_change_Role_patch
    assert_equal 200, @patchcode
    assert_equal @postbody['User'][0]['ID'], @patchbody['User'][0]['ID']
    assert_equal @postbody['User'][0]['Role'], @patchbody['User'][0]['Role']
  end
end

class TestUserCreateDisabledUserAndAuthXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @code, @headers, @body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @user_patch = { :Status => ['disabled'] }
    @patchcode, @patchheaders, @patchbody = payl_request_xml(:patch, "/users/#{@body['User'][0]['ID'][0]}", ADMIN_KEY_XML, @user_patch, 'User')

    @authcode, @authheaders, @authbody = auth_request_xml(@user_payl[:Login][0], @user_payl[:Password][0])
  end

  def test_create_disabled_user_and_auth
    assert_equal 401, @authcode
    assert_equal 'application/xml', @authheaders['content-type'][0]
    assert_equal ['unauthorized'], @authbody['Error']
  end
end

class TestUserCreateDeletedUserAndAuthXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @code, @headers, @body = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    @user_patch = { :Status => ['deleted'] }
    @patchcode, @patchheaders, @patchbody = payl_request_xml(:patch, "/users/#{@body['User'][0]['ID'][0]}", ADMIN_KEY_XML, @user_patch, 'User')

    @authcode, @authheaders, @authbody = auth_request_xml(@user_payl[:Login][0], @user_payl[:Password][0])
  end

  def test_create_deleted_user_and_auth
    assert_equal 401, @authcode
    assert_equal 'application/xml', @authheaders['content-type'][0]
    assert_equal ['unauthorized'], @authbody['Error']
  end
end

class TestUserGetUsersThenDisableXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    user_key, user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])
    @getcode, @getheaders, @getbody = get_request_xml('/users', user_key)

    @user_patch = { :Status => ['disabled'] }
    @patchcode, @patchheaders, @patchbody = payl_request_xml(:patch, "/users/#{@postbody['User'][0]['ID'][0]}", ADMIN_KEY_XML, @user_patch, 'User')

    @discode, @disheaders, @disbody = get_request_xml('/users', user_key)
  end

  def test_user_single_user_then_disable
    assert_equal 200, @getcode
    assert_equal 1, @getbody['Users'][0]['User'].length
    assert_equal @postbody['User'][0]['ID'], @getbody['Users'][0]['User'][0]['ID']

    assert_equal 401, @discode
    assert_equal 'application/xml', @disheaders['content-type'][0]
    assert_equal ['unauthorized'], @disbody['Error']
  end
end

class TestUserGetUsersThenDeleteXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'User')

    user_key, user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])
    @getcode, @getheaders, @getbody = get_request_xml('/users', user_key)

    @user_patch = { :Status => ['deleted'] }
    @patchcode, @patchheaders, @patchbody = payl_request_xml(:patch, "/users/#{@postbody['User'][0]['ID'][0]}", ADMIN_KEY_XML, @user_patch, 'User')

    @discode, @disheaders, @disbody = get_request_xml('/users', user_key)
  end

  def test_user_single_user_then_delete
    assert_equal 200, @getcode
    assert_equal 1, @getbody['Users'][0]['User'].length
    assert_equal @postbody['User'][0]['ID'], @getbody['Users'][0]['User'][0]['ID']

    assert_equal 401, @discode
    assert_equal 'application/xml', @disheaders['content-type'][0]
    assert_equal ['unauthorized'], @disbody['Error']
  end
end
