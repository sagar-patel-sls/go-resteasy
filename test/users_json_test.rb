require 'minitest/autorun'
require_relative 'helpers'

class TestCreateUserJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @code, @headers, @body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
  end

  def test_user_created
    assert_equal 201, @code
    assert_equal 'application/json', @headers['content-type'][0]
    refute_nil @body['id']
    assert_equal @user_payl[:role], @body['role']
    assert_equal @user_payl[:email], @body['email']
    assert_equal 'active', @body['status']
    assert_equal "/users/#{@body['id']}", @body['links'][0]['href']
  end
end

class TestCreateUserSubtreeJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @code, @headers, @body = payl_request_json(:post, '/users/', ADMIN_KEY_JSON, @user_payl)
  end

  def test_user_created_subtree
    assert_equal 201, @code
    assert_equal 'application/json', @headers['content-type'][0]
    refute_nil @body['id']
    assert_equal @user_payl[:role], @body['role']
    assert_equal @user_payl[:email], @body['email']
    assert_equal 'active', @body['status']
    assert_equal "/users/#{@body['id']}", @body['links'][0]['href']
  end
end

class TestCreateUserBadPayloadJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @code, @headers, @body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl.to_json[0..-5], true)
  end

  def test_incomplete_json
    assert_equal 400, @code
    assert_equal 'unexpected EOF', @body['error']
  end
end

class TestCreateUserMissingFieldsJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @user_payl.delete(:email)
    @code, @headers, @body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
  end

  def test_missing_email
    assert_equal 400, @code
    assert_equal 'required field missing: email', @body['error']
  end
end

class TestCreateAndGetUserJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @getcode, @getheaders, @getbody = get_request_json("/users/#{@postbody['id']}", ADMIN_KEY_JSON)
  end

  def test_user_created_and_retrieved
    assert_equal 200, @getcode
    refute_nil @getbody['id']
    assert_equal @user_payl[:role], @getbody['role']
    assert_equal @user_payl[:email], @getbody['email']
    assert_equal 'active', @getbody['status']
    assert_equal "/users/#{@getbody['id']}", @getbody['links'][0]['href']
  end
end

class TestCreateAndGetAllUsersJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @post1code, @post1headers, @post1body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @user_payl = default_user_json
    @post2code, @post2headers, @post2body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @user_payl = default_user_json
    @post3code, @post3headers, @post3body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    # Verify all 3 users show in the /users list after creation
    @ids = []
    @allcode, @allheaders, @allbody = get_request_json("/users", ADMIN_KEY_JSON)
    @allbody.each do |user|
      @ids << user['id']
    end
  end

  def test_user_all_created
    assert_equal 200, @allcode
    assert @ids.include?(@post1body['id'])
    assert @ids.include?(@post2body['id'])
    assert @ids.include?(@post3body['id'])
  end
end

class TestCreateAndUpdateUserJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @user_upd = @user_payl
    @user_upd[:email] = "#{SecureRandom.uuid}@example.com"
    @user_upd[:status] = @postbody['status']
    @putcode, @putheaders, @putbody = payl_request_json(:put, "/users/#{@postbody['id']}", ADMIN_KEY_JSON, @user_upd)
  end

  def test_user_created_and_updated
    assert_equal 200, @putcode
    assert_equal @postbody['id'], @putbody['id']
    refute_equal @postbody['email'], @putbody['email']
    assert_equal @user_upd[:email], @putbody['email']
  end
end

class TestCreateAndPatchUserJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @user_patch = { :email => "#{SecureRandom.uuid}@example.com" }
    @patchcode, @patchheaders, @patchbody = payl_request_json(:patch, "/users/#{@postbody['id']}", ADMIN_KEY_JSON, @user_patch)
  end

  def test_user_created_and_patched
    assert_equal 200, @patchcode
    assert_equal @postbody['id'], @patchbody['id']
    refute_equal @postbody['email'], @patchbody['email']
    assert_equal @user_patch[:email], @patchbody['email']
  end
end

class TestCreateAndDeleteUserJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @delcode, @delheaders, @delbody = del_request_json("/users/#{@postbody['id']}", ADMIN_KEY_JSON)

    @getcode, @getheaders, @getbody = get_request_json("/users/#{@postbody['id']}", ADMIN_KEY_JSON)

    # Verify the user no longer shows in the /users list after deletion
    @ids = []
    @allcode, @allheaders, @allbody = get_request_json("/users", ADMIN_KEY_JSON)
    @allbody.each do |user|
      @ids << user['id']
    end
  end

  def test_user_created_and_deleted
    assert_equal 204, @delcode
    assert_nil @delbody

    assert_equal 200, @getcode
    assert_equal 'deleted', @getbody['status']

    assert_equal 200, @allcode
    refute @ids.include?(@postbody['id'])
  end
end

class TestGetUserBadFormatIdJson < Minitest::Test
  def setup
    @code, @headers, @body = get_request_json('/users/not-a-uuid', ADMIN_KEY_JSON)
  end

  def test_parse_id_failure
    assert_equal 400, @code
    assert_equal 'ERROR: invalid input syntax for type uuid: "not-a-uuid" (SQLSTATE 22P02)', @body['error']
  end
end

class TestGetUserIdNotFoundJson < Minitest::Test
  def setup
    @code, @headers, @body = get_request_json('/users/00000000-0000-9000-0000-000000000000', ADMIN_KEY_JSON)
  end

  def test_id_not_found
    assert_equal 404, @code
    assert_equal 'not found', @body['error']
  end
end

class TestUserCreateUserJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @code, @headers, @body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    user_key, user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])
    @uu_payl = default_user_json
    @usercode, @userheaders, @userbody = payl_request_json(:post, '/users', user_key, @uu_payl)
  end

  def test_user_forbidden
    assert_equal 403, @usercode
    assert_equal 'forbidden', @userbody['error']
  end
end

class TestUserGetUsersJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    user_key, user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])
    @getcode, @getheaders, @getbody = get_request_json('/users', user_key)
  end

  def test_user_single_user
    assert_equal 200, @getcode
    assert_equal 1, @getbody.length
    assert_equal @postbody['id'], @getbody[0]['id']
  end
end

class TestUserDelUserJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    user_key, user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])
    @delcode, @delheaders, @delbody = del_request_json("/users/#{@postbody['id']}", user_key)
  end

  def test_user_fail_del_self
    assert_equal 403, @delcode
    assert_equal 'forbidden', @delbody['error']
  end
end

class TestUserGetDifferentUserJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @user1code, @user1headers, @user1body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
    user1_key, user1_id = user_key_id_json(@user_payl[:login], @user_payl[:password])

    @user_payl = default_user_json
    @user2code, @user2headers, @user2body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @getcode, @getheaders, @getbody = get_request_json("/users/#{@user2body['id']}", user1_key)
  end

  def test_user_fail_get_different_user
    assert_equal 404, @getcode
    assert_equal 'not found', @getbody['error']
  end
end

class TestUserUpdateDifferentUserJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @user1code, @user1headers, @user1body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
    user1_key, user1_id = user_key_id_json(@user_payl[:login], @user_payl[:password])

    @user_payl = default_user_json
    @user2code, @user2headers, @user2body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @user_upd = @user_payl
    @user_upd[:email] = "#{SecureRandom.uuid}@example.com"
    @putcode, @putheaders, @putbody = payl_request_json(:put, "/users/#{@user2body['id']}", user1_key, @user_upd)
  end

  def test_user_fail_update_different_user
    assert_equal 404, @putcode
    assert_equal 'not found', @putbody['error']
  end
end

class TestUserPatchDifferentUserJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @user1code, @user1headers, @user1body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
    user1_key, user1_id = user_key_id_json(@user_payl[:login], @user_payl[:password])

    @user_payl = default_user_json
    @user2code, @user2headers, @user2body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @user_patch = { :email => "#{SecureRandom.uuid}@example.com" }
    @patchcode, @patchheaders, @patchbody = payl_request_json(:patch, "/users/#{@user2body['id']}", user1_key, @user_patch)
  end

  def test_user_fail_patch_different_user
    assert_equal 404, @patchcode
    assert_equal 'not found', @patchbody['error']
  end
end

class TestAdminChangeUserTypePutJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @user_upd = @user_payl
    @user_upd[:role] = 'admin'
    @user_upd[:status] = @postbody['status']
    @putcode, @putheaders, @putbody = payl_request_json(:put, "/users/#{@postbody['id']}", ADMIN_KEY_JSON, @user_upd)
  end

  def test_admin_change_role_patch
    assert_equal 200, @putcode
    assert_equal @postbody['id'], @putbody['id']
    refute_equal @postbody['role'], @putbody['role']
    assert_equal @user_upd[:role], @putbody['role']
  end
end

class TestUserChangeUserTypePutJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
    user_key, user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])

    @user_upd = @user_payl
    @user_upd[:role] = 'admin'
    @user_upd[:status] = @postbody['status']
    @putcode, @putheaders, @putbody = payl_request_json(:put, "/users/#{@postbody['id']}", user_key, @user_upd)
  end

  def test_user_change_role_patch
    assert_equal 200, @putcode
    assert_equal @postbody['id'], @putbody['id']
    assert_equal @postbody['role'], @putbody['role']
  end
end

class TestAdminChangeUserTypePatchJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @user_patch = { :role => 'admin' }
    @patchcode, @patchheaders, @patchbody = payl_request_json(:patch, "/users/#{@postbody['id']}", ADMIN_KEY_JSON, @user_patch)
  end

  def test_admin_change_role_patch
    assert_equal 200, @patchcode
    assert_equal @postbody['id'], @patchbody['id']
    refute_equal @postbody['role'], @patchbody['role']
    assert_equal @user_patch[:role], @patchbody['role']
  end
end

class TestUserChangeUserTypePatchJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
    user_key, user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])

    @user_patch = { :role => 'admin' }
    @patchcode, @patchheaders, @patchbody = payl_request_json(:patch, "/users/#{@postbody['id']}", user_key, @user_patch)
  end

  def test_user_change_role_patch
    assert_equal 200, @patchcode
    assert_equal @postbody['id'], @patchbody['id']
    assert_equal @postbody['role'], @patchbody['role']
  end
end

class TestUserCreateDisabledUserAndAuthJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @code, @headers, @body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @user_patch = { :status => 'disabled' }
    @patchcode, @patchheaders, @patchbody = payl_request_json(:patch, "/users/#{@body['id']}", ADMIN_KEY_JSON, @user_patch)

    @authcode, @authheaders, @authbody = auth_request_json(@user_payl[:login], @user_payl[:password])
  end

  def test_create_disabled_user_and_auth
    assert_equal 401, @authcode
    assert_equal 'application/json', @authheaders['content-type'][0]
    assert_equal 'unauthorized', @authbody['error']
  end
end

class TestUserCreateDeletedUserAndAuthJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @code, @headers, @body = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    @user_patch = { :status => 'deleted' }
    @patchcode, @patchheaders, @patchbody = payl_request_json(:patch, "/users/#{@body['id']}", ADMIN_KEY_JSON, @user_patch)

    @authcode, @authheaders, @authbody = auth_request_json(@user_payl[:login], @user_payl[:password])
  end

  def test_create_deleted_user_and_auth
    assert_equal 401, @authcode
    assert_equal 'application/json', @authheaders['content-type'][0]
    assert_equal 'unauthorized', @authbody['error']
  end
end

class TestUserGetUsersThenDisableJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    user_key, user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])
    @getcode, @getheaders, @getbody = get_request_json('/users', user_key)

    @user_patch = { :status => 'disabled' }
    @patchcode, @patchheaders, @patchbody = payl_request_json(:patch, "/users/#{@postbody['id']}", ADMIN_KEY_JSON, @user_patch)

    @discode, @disheaders, @disbody = get_request_json('/users', user_key)
  end

  def test_user_single_user_then_disable
    assert_equal 200, @getcode
    assert_equal 1, @getbody.length
    assert_equal @postbody['id'], @getbody[0]['id']

    assert_equal 401, @discode
    assert_equal 'application/json', @disheaders['content-type'][0]
    assert_equal 'unauthorized', @disbody['error']
  end
end

class TestUserGetUsersThenDeleteJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)

    user_key, user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])
    @getcode, @getheaders, @getbody = get_request_json('/users', user_key)

    @user_patch = { :status => 'deleted' }
    @patchcode, @patchheaders, @patchbody = payl_request_json(:patch, "/users/#{@postbody['id']}", ADMIN_KEY_JSON, @user_patch)

    @discode, @disheaders, @disbody = get_request_json('/users', user_key)
  end

  def test_user_single_user_then_delete
    assert_equal 200, @getcode
    assert_equal 1, @getbody.length
    assert_equal @postbody['id'], @getbody[0]['id']

    assert_equal 401, @discode
    assert_equal 'application/json', @disheaders['content-type'][0]
    assert_equal 'unauthorized', @disbody['error']
  end
end
