require 'minitest/autorun'
require_relative 'helpers'

class TestCreateJobXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @code, @headers, @body = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')
  end

  def test_job_created
    assert_equal 201, @code
    assert_equal 'application/xml', @headers['content-type'][0]
    refute_nil @body['Job'][0]['ID'][0]
    assert_equal ADMIN_ID_XML, @body['Job'][0]['UserID'][0]
    assert_equal @job_payl[:StartedAt][0], @body['Job'][0]['StartedAt'][0]
    assert_equal ['ready'], @body['Job'][0]['Status']
    assert_equal "/jobs/#{@body['Job'][0]['ID'][0]}", @body['Job'][0]['Links'][0]['Link'][0]['href']
  end
end

class TestCreateJobSubtreeXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @code, @headers, @body = payl_request_xml(:post, '/jobs/', ADMIN_KEY_XML, @job_payl, 'Job')
  end

  def test_job_created_subtree
    assert_equal 201, @code
    assert_equal 'application/xml', @headers['content-type'][0]
    refute_nil @body['Job'][0]['ID'][0]
    assert_equal ADMIN_ID_XML, @body['Job'][0]['UserID'][0]
    assert_equal @job_payl[:StartedAt][0], @body['Job'][0]['StartedAt'][0]
    assert_equal ['ready'], @body['Job'][0]['Status']
    assert_equal "/jobs/#{@body['Job'][0]['ID'][0]}", @body['Job'][0]['Links'][0]['Link'][0]['href']
  end
end

class TestCreateJobBadPayloadXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @code, @headers, @body = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, XmlSimple.xml_out(@job_payl, 'RootName' => 'Job')[0..-5], 'Job', true)
  end

  def test_incomplete_xml
    assert_equal 400, @code
    assert_equal ['XML syntax error on line 4: unexpected EOF'], @body['Error']
  end
end

# class TestCreateJobMissingFieldsXml < Minitest::Test
#   def setup
#     @job_payl = default_job_xml
#     @job_payl.delete()
#     @code, @headers, @body = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl)
#   end

#   def test_missing_
#     assert_equal 400, @code
#     assert_equal ['Required field missing or invalid: '], @body['Error']
#   end
# end

class TestCreateAndGetJobXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')

    @getcode, @getheaders, @getbody = get_request_xml("/jobs/#{@postbody['Job'][0]['ID'][0]}", ADMIN_KEY_XML)
  end

  def test_job_created_and_retrieved
    assert_equal 200, @getcode
    refute_nil @getbody['Job'][0]['ID'][0]
    assert_equal ADMIN_ID_XML, @getbody['Job'][0]['UserID'][0]
    assert_equal @job_payl[:StartedAt][0], @getbody['Job'][0]['StartedAt'][0]
    assert_equal ['ready'], @getbody['Job'][0]['Status']
    assert_equal "/jobs/#{@postbody['Job'][0]['ID'][0]}", @getbody['Job'][0]['Links'][0]['Link'][0]['href']
  end
end

class TestCreateAndGetAllJobsXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @post1code, @post1headers, @post1body = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')

    @job_payl = default_job_xml
    @post2code, @post2headers, @post2body = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')

    @job_payl = default_job_xml
    @post3code, @post3headers, @post3body = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')

    # Verify all 3 jobs show in the /jobs list after creation
    @ids = []
    @allcode, @allheaders, @allbody = get_request_xml('/jobs', ADMIN_KEY_XML)
    @allbody['Jobs'][0]['Job'].each do |job|
      @ids << job['ID'][0]
    end
  end

  def test_job_all_created
    assert_equal 200, @allcode
    assert @ids.include?(@post1body['Job'][0]['ID'][0])
    assert @ids.include?(@post2body['Job'][0]['ID'][0])
    assert @ids.include?(@post3body['Job'][0]['ID'][0])
  end
end

class TestCreateAndUpdateJobXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')

    @job_upd = @job_payl
    @job_upd[:CompletedAt] = [Time.new(2057).utc.iso8601]
    @putcode, @putheaders, @putbody = payl_request_xml(:put, "/jobs/#{@postbody['Job'][0]['ID'][0]}", ADMIN_KEY_XML, @job_upd, 'Job')
  end

  def test_job_created_and_updated
    assert_equal 200, @putcode
    assert_equal @postbody['Job'][0]['ID'][0], @putbody['Job'][0]['ID'][0]
    refute_equal @postbody['Job'][0]['CompletedAt'][0], @putbody['Job'][0]['CompletedAt'][0]
    assert_equal @job_upd[:CompletedAt][0], @putbody['Job'][0]['CompletedAt'][0]
  end
end

class TestCreateAndPatchJobXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')

    @job_patch = { :CompletedAt => [Time.new(2057).utc.iso8601] }
    @patchcode, @patchheaders, @patchbody = payl_request_xml(:patch, "/jobs/#{@postbody['Job'][0]['ID'][0]}", ADMIN_KEY_XML, @job_patch, 'Job')
  end

  def test_job_created_and_patched
    assert_equal 200, @patchcode
    assert_equal @postbody['Job'][0]['ID'][0], @patchbody['Job'][0]['ID'][0]
    refute_equal @postbody['Job'][0]['CompletedAt'][0], @patchbody['Job'][0]['CompletedAt'][0]
    assert_equal @job_patch[:CompletedAt][0], @patchbody['Job'][0]['CompletedAt'][0]
  end
end

class TestCreateAndDeleteJobXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @postcode, @postheaders, @postbody = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')

    @delcode, @delheaders, @delbody = del_request_xml("/jobs/#{@postbody['Job'][0]['ID'][0]}", ADMIN_KEY_XML)

    @getcode, @getheaders, @getbody = get_request_xml("/jobs/#{@postbody['Job'][0]['ID'][0]}", ADMIN_KEY_XML)

    # Verify the job no longer shows in the /jobs list after deletion
    @ids = []
    @allcode, @allheaders, @allbody = get_request_xml('/jobs', ADMIN_KEY_XML)
    @allbody['Jobs'][0]['Job'].each do |job|
      @ids << job['ID'][0]
    end
  end

  def test_job_created_and_deleted
    assert_equal 204, @delcode
    assert_nil @delbody

    assert_equal 200, @getcode
    assert_equal ['deleted'], @getbody['Job'][0]['Status']

    assert_equal 200, @allcode
    refute @ids.include?(@postbody['Job'][0]['ID'][0])
  end
end

class TestGetJobBadFormatIdXml < Minitest::Test
  def setup
    @code, @headers, @body = get_request_xml('/jobs/not-a-uuid', ADMIN_KEY_XML)
  end

  def test_parse_id_failure
    assert_equal 400, @code
    assert_equal ['ERROR: invalid input syntax for type uuid: "not-a-uuid" (SQLSTATE 22P02)'], @body['Error']
  end
end

class TestGetJobIdNotFoundXml < Minitest::Test
  def setup
    @code, @headers, @body = get_request_xml('/jobs/00000000-0000-9000-0000-000000000000', ADMIN_KEY_XML)
  end

  def test_id_not_found
    assert_equal 404, @code
    assert_equal ['not found'], @body['Error']
  end
end

class TestUserGetJobsXml < Minitest::Test
  def setup
    @user_payl = default_user_xml
    @usercode, @userheaders, @userbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'user')
    user_key, @user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])

    @job_payl = default_job_xml
    @jobcode, @jobheaders, @jobbody = payl_request_xml(:post, '/jobs', user_key, @job_payl, 'Job')

    @getcode, @getheaders, @getbody = get_request_xml('/jobs', user_key)
  end

  def test_user_single_job
    assert_equal 200, @getcode
    assert_equal 1, @getbody['Jobs'][0]['Job'].length
    assert_equal @user_id, @getbody['Jobs'][0]['Job'][0]['UserID'][0]
    assert_equal @jobbody['Job'][0]['ID'][0], @getbody['Jobs'][0]['Job'][0]['ID'][0]
  end
end

class TestUserGetDifferentUserJobXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @jobcode, @jobheaders, @jobbody = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')

    @user_payl = default_user_xml
    @usercode, @userheaders, @userbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'user')
    user_key, user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])

    @getcode, @getheaders, @getbody = get_request_xml("/jobs/#{@jobbody['Job'][0]['ID'][0]}", user_key)
  end

  def test_user_fail_get_different_user_job
    assert_equal 404, @getcode
    assert_equal ['not found'], @getbody['Error']
  end
end

class TestUserDelDifferentUserJobXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @jobcode, @jobheaders, @jobbody = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')

    @user_payl = default_user_xml
    @usercode, @userheaders, @userbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'user')
    user_key, user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])

    @getcode, @getheaders, @getbody = del_request_xml("/jobs/#{@jobbody['Job'][0]['ID'][0]}", user_key)
  end

  def test_user_fail_del_different_user_job
    assert_equal 404, @getcode
    assert_equal ['not found'], @getbody['Error']
  end
end

class TestUserUpdateDifferentUserJobXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @jobcode, @jobheaders, @jobbody = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')

    @user_payl = default_user_xml
    @usercode, @userheaders, @userbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'user')
    user_key, user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])

    @job_upd = @job_payl
    @job_upd[:CompletedAt] = [Time.new(2057).utc.iso8601]
    @putcode, @putheaders, @putbody = payl_request_xml(:put, "/jobs/#{@jobbody['Job'][0]['ID'][0]}", user_key, @job_upd, 'Job')
  end

  def test_user_fail_update_different_user_job
    assert_equal 404, @putcode
    assert_equal ['not found'], @putbody['Error']
  end
end

class TestUserPatchDifferentUserJobXml < Minitest::Test
  def setup
    @job_payl = default_job_xml
    @jobcode, @jobheaders, @jobbody = payl_request_xml(:post, '/jobs', ADMIN_KEY_XML, @job_payl, 'Job')

    @user_payl = default_user_xml
    @usercode, @userheaders, @userbody = payl_request_xml(:post, '/users', ADMIN_KEY_XML, @user_payl, 'user')
    user_key, user_id = user_key_id_xml(@user_payl[:Login][0], @user_payl[:Password][0])

    @job_patch = { :CompletedAt => [Time.new(2057).utc.iso8601] }
    @patchcode, @patchheaders, @patchbody = payl_request_xml(:patch, "/jobs/#{@jobbody['Job'][0]['ID'][0]}", user_key, @job_patch, 'Job')
  end

  def test_user_fail_patch_different_user_job
    assert_equal 404, @patchcode
    assert_equal ['not found'], @patchbody['Error']
  end
end
