require 'minitest/autorun'
require_relative 'helpers'

class TestCreateJobJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @code, @headers, @body = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)
  end

  def test_job_created
    assert_equal 201, @code
    assert_equal 'application/json', @headers['content-type'][0]
    refute_nil @body['id']
    assert_equal ADMIN_ID_JSON, @body['userId']
    assert_equal @job_payl[:startedAt], @body['startedAt']
    assert_equal 'ready', @body['status']
    assert_equal "/jobs/#{@body['id']}", @body['links'][0]['href']
  end
end

class TestCreateJobSubtreeJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @code, @headers, @body = payl_request_json(:post, '/jobs/', ADMIN_KEY_JSON, @job_payl)
  end

  def test_job_created_subtree
    assert_equal 201, @code
    assert_equal 'application/json', @headers['content-type'][0]
    refute_nil @body['id']
    assert_equal ADMIN_ID_JSON, @body['userId']
    assert_equal @job_payl[:startedAt], @body['startedAt']
    assert_equal 'ready', @body['status']
    assert_equal "/jobs/#{@body['id']}", @body['links'][0]['href']
  end
end

class TestCreateJobBadPayloadJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @code, @headers, @body = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl.to_json[0..-5], true)
  end

  def test_incomplete_json
    assert_equal 400, @code
    assert_equal 'unexpected EOF', @body['error']
  end
end

# class TestCreateJobMissingFieldsJson < Minitest::Test
#   def setup
#     @job_payl = default_job_json
#     @job_payl.delete()
#     @code, @headers, @body = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)
#   end

#   def test_missing_
#     assert_equal 400, @code
#     assert_equal 'Required field missing or invalid: ', @body['error']
#   end
# end

class TestCreateAndGetJobJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)

    @getcode, @getheaders, @getbody = get_request_json("/jobs/#{@postbody['id']}", ADMIN_KEY_JSON)
  end

  def test_job_created_and_retrieved
    assert_equal 200, @getcode
    refute_nil @getbody['id']
    assert_equal ADMIN_ID_JSON, @getbody['userId']
    assert_equal @job_payl[:completedAt], @getbody['completedAt']
    assert_equal 'ready', @getbody['status']
    assert_equal "/jobs/#{@getbody['id']}", @getbody['links'][0]['href']
  end
end

class TestCreateAndGetAllJobsJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @post1code, @post1headers, @post1body = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)

    @job_payl = default_job_json
    @post2code, @post2headers, @post2body = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)

    @job_payl = default_job_json
    @post3code, @post3headers, @post3body = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)

    # Verify all 3 jobs show in the /jobs list after creation
    @ids = []
    @allcode, @allheaders, @allbody = get_request_json('/jobs', ADMIN_KEY_JSON)
    @allbody.each do |job|
      @ids << job['id']
    end
  end

  def test_job_all_created
    assert_equal 200, @allcode
    assert @ids.include?(@post1body['id'])
    assert @ids.include?(@post2body['id'])
    assert @ids.include?(@post3body['id'])
  end
end

class TestCreateAndUpdateJobJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)

    @job_upd = @job_payl
    @job_upd[:completedAt] = Time.new(2057).utc.iso8601
    @putcode, @putheaders, @putbody = payl_request_json(:put, "/jobs/#{@postbody['id']}", ADMIN_KEY_JSON, @job_upd)
  end

  def test_job_created_and_updated
    assert_equal 200, @putcode
    assert_equal @postbody['id'], @putbody['id']
    refute_equal @postbody['completedAt'], @putbody['completedAt']
    assert_equal @job_upd[:completedAt], @putbody['completedAt']
  end
end

class TestCreateAndPatchJobJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)

    @job_patch = { :completedAt => Time.new(2057).utc.iso8601 }
    @patchcode, @patchheaders, @patchbody = payl_request_json(:patch, "/jobs/#{@postbody['id']}", ADMIN_KEY_JSON, @job_patch)
  end

  def test_job_created_and_patched
    assert_equal 200, @patchcode
    assert_equal @postbody['id'], @patchbody['id']
    refute_equal @postbody['completedAt'], @patchbody['completedAt']
    assert_equal @job_patch[:completedAt], @patchbody['completedAt']
  end
end

class TestCreateAndDeleteJobJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @postcode, @postheaders, @postbody = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)

    @delcode, @delheaders, @delbody = del_request_json("/jobs/#{@postbody['id']}", ADMIN_KEY_JSON)

    @getcode, @getheaders, @getbody = get_request_json("/jobs/#{@postbody['id']}", ADMIN_KEY_JSON)

    # Verify the job no longer shows in the /jobs list after deletion
    @ids = []
    @allcode, @allheaders, @allbody = get_request_json('/jobs', ADMIN_KEY_JSON)
    @allbody.each do |job|
      @ids << job['id']
    end
  end

  def test_job_created_and_deleted
    assert_equal 204, @delcode
    assert_nil @delbody

    assert_equal 200, @getcode
    assert_equal 'deleted', @getbody['status']

    assert_equal 200, @allcode
    refute @ids.include?(@postbody['id'])
  end
end

class TestGetJobBadFormatIdJson < Minitest::Test
  def setup
    @code, @headers, @body = get_request_json('/jobs/not-a-uuid', ADMIN_KEY_JSON)
  end

  def test_parse_id_failure
    assert_equal 400, @code
    assert_equal 'ERROR: invalid input syntax for type uuid: "not-a-uuid" (SQLSTATE 22P02)', @body['error']
  end
end

class TestGetJobIdNotFoundJson < Minitest::Test
  def setup
    @code, @headers, @body = get_request_json('/jobs/00000000-0000-9000-0000-000000000000', ADMIN_KEY_JSON)
  end

  def test_id_not_found
    assert_equal 404, @code
    assert_equal 'not found', @body['error']
  end
end

class TestUserGetJobsJson < Minitest::Test
  def setup
    @user_payl = default_user_json
    @usercode, @userheaders, @userbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
    user_key, @user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])

    @job_payl = default_job_json
    @jobcode, @jobheaders, @jobbody = payl_request_json(:post, '/jobs', user_key, @job_payl)

    @getcode, @getheaders, @getbody = get_request_json('/jobs', user_key)
  end

  def test_user_single_job
    assert_equal 200, @getcode
    assert_equal 1, @getbody.length
    assert_equal @user_id, @getbody[0]['userId']
    assert_equal @jobbody['id'], @getbody[0]['id']
  end
end

class TestUserGetDifferentUserJobJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @jobcode, @jobheaders, @jobbody = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)

    @user_payl = default_user_json
    @usercode, @userheaders, @userbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
    user_key, user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])

    @getcode, @getheaders, @getbody = get_request_json("/jobs/#{@jobbody['id']}", user_key)
  end

  def test_user_fail_get_different_user_job
    assert_equal 404, @getcode
    assert_equal 'not found', @getbody['error']
  end
end

class TestUserDelDifferentUserJobJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @jobcode, @jobheaders, @jobbody = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)

    @user_payl = default_user_json
    @usercode, @userheaders, @userbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
    user_key, user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])

    @getcode, @getheaders, @getbody = del_request_json("/jobs/#{@jobbody['id']}", user_key)
  end

  def test_user_fail_del_different_user_job
    assert_equal 404, @getcode
    assert_equal 'not found', @getbody['error']
  end
end

class TestUserUpdateDifferentUserJobJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @jobcode, @jobheaders, @jobbody = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)

    @user_payl = default_user_json
    @usercode, @userheaders, @userbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
    user_key, user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])

    @job_upd = @job_payl
    @job_upd[:completedAt] = Time.new(2057).utc.iso8601
    @putcode, @putheaders, @putbody = payl_request_json(:put, "/jobs/#{@jobbody['id']}", user_key, @job_upd)
  end

  def test_user_fail_update_different_user_job
    assert_equal 404, @putcode
    assert_equal 'not found', @putbody['error']
  end
end

class TestUserPatchDifferentUserJobJson < Minitest::Test
  def setup
    @job_payl = default_job_json
    @jobcode, @jobheaders, @jobbody = payl_request_json(:post, '/jobs', ADMIN_KEY_JSON, @job_payl)

    @user_payl = default_user_json
    @usercode, @userheaders, @userbody = payl_request_json(:post, '/users', ADMIN_KEY_JSON, @user_payl)
    user_key, user_id = user_key_id_json(@user_payl[:login], @user_payl[:password])

    @job_patch = { :completedAt => Time.new(2057).utc.iso8601 }
    @patchcode, @patchheaders, @patchbody = payl_request_json(:patch, "/jobs/#{@jobbody['id']}", user_key, @job_patch)
  end

  def test_user_fail_patch_different_user_job
    assert_equal 404, @patchcode
    assert_equal 'not found', @patchbody['error']
  end
end
