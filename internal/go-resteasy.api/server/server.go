package server

import (
	"log"
	"net/http"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/config"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/controller"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/store"
	"gitlab.com/bconway/go-resteasy/pkg/postgres"
)

// New instantiates a store, controllers, and HTTP server.
func New(cfg *config.Config) *http.Server {
	// Build the store.
	db, err := postgres.New(cfg.PgURI)
	if err != nil {
		log.Fatalf("New postgres.New: %#v", err)
	}
	s := &store.Store{DB: db}

	// Instantiate controllers.
	auth := &controller.Auth{Store: s}
	user := &controller.User{Store: s}
	job := &controller.Job{Store: s}

	// For the specifics of path and subtree matching, see:
	// https://golang.org/pkg/net/http/#ServeMux
	smux := http.NewServeMux()
	smux.Handle("/", controller.RootHandler())
	smux.Handle("/auth", http.StripPrefix("/auth", auth.AuthHandler()))
	smux.Handle("/users", http.StripPrefix("/users", user.UserHandler()))
	smux.Handle("/users/", http.StripPrefix("/users/", user.UserHandler()))
	smux.Handle("/jobs", http.StripPrefix("/jobs", job.JobHandler()))
	smux.Handle("/jobs/", http.StripPrefix("/jobs/", job.JobHandler()))
	return &http.Server{Addr: ":8080", Handler: smux}
}
