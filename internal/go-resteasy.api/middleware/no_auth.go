package middleware

import (
	"net/http"
	"strings"
	"time"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/response"
)

// NoAuth satisfies the Handler interface and runs RunFunc and logs.
type NoAuth struct {
	RunFunc func(response.FormatWriter, *http.Request)
}

// ServeHTTP satisfies the Handler interface.
func (n *NoAuth) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var fw response.FormatWriter

	// Prefer application/json by default.
	accept := r.Header.Get("Accept")
	switch {
	case strings.Contains(accept, "application/xml"):
		fw = response.NewXMLWriter(w, time.Now(), "noauth", "noauth")
		fw.Header().Set("Content-Type", "application/xml")
	default:
		fw = response.NewJSONWriter(w, time.Now(), "noauth", "noauth")
		fw.Header().Set("Content-Type", "application/json")
	}

	n.RunFunc(fw, r)
	logHandlerFunc(fw, r)
}
