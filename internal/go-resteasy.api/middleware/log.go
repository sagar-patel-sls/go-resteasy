package middleware

import (
	"log"
	"net/http"
	"time"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/response"
)

// logHandlerFunc logs requests and calculates performance.
func logHandlerFunc(fw response.FormatWriter, r *http.Request) {
	log.Printf("%s %s %s %s %s %d %dms", r.RemoteAddr, fw.User(), fw.UserType(),
		r.Method, r.RequestURI, fw.StatusCode(),
		time.Since(fw.StartTime())/time.Millisecond)
}
