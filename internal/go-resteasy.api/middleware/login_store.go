package middleware

import (
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/model"
)

// loginStore defines the methods used by an authentication middleware.
type loginStore interface {
	ReadUserByLogin(login string) (*model.User, error)
	ReadUserByKey(key string) (*model.User, error)
}
