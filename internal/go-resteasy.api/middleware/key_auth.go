package middleware

import (
	"log"
	"net/http"
	"strings"
	"time"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/model"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/response"
)

// KeyAuth satisfies the Handler interface and performs basic auth, runs
// RunFunc, and logs.
type KeyAuth struct {
	Store   loginStore
	RunFunc func(response.FormatWriter, *http.Request, *model.User)
}

// ServeHTTP satisfies the Handler interface.
func (k *KeyAuth) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var fw response.FormatWriter

	// Prefer application/json by default.
	accept := r.Header.Get("Accept")
	switch {
	case strings.Contains(accept, "application/xml"):
		fw = response.NewXMLWriter(w, time.Now(), "preauth", "preauth")
		fw.Header().Set("Content-Type", "application/xml")
	default:
		fw = response.NewJSONWriter(w, time.Now(), "preauth", "preauth")
		fw.Header().Set("Content-Type", "application/json")
	}

	k.AuthRun(fw, r)
	logHandlerFunc(fw, r)
}

// AuthRun authenticates by key and runs RunFunc if successful.
func (k *KeyAuth) AuthRun(fw response.FormatWriter, r *http.Request) {
	if r.Header.Get("Authorization") == "" {
		response.Unauthorized(fw, r)
		return
	}

	var key string
	auth := strings.Split(r.Header.Get("Authorization"), " ")
	if len(auth) == 2 && auth[0] == "Bearer" {
		key = auth[1]
	} else {
		response.Unauthorized(fw, r)
		return
	}

	if client, err := k.Store.ReadUserByKey(key); err != nil {
		log.Printf("Login failure, GetUserByKey: %s - %v", key, err)
		response.Unauthorized(fw, r)
	} else {
		fw.SetUser("key:" + client.Login)
		fw.SetUserType(client.Role)
		k.RunFunc(fw, r, client)
	}
}
