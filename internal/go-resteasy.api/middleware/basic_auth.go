package middleware

import (
	"log"
	"net/http"
	"strings"
	"time"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/model"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/response"
	"golang.org/x/crypto/bcrypt"
)

// BasicAuth satisfies the Handler interface and performs basic auth, runs
// RunFunc, and logs.
type BasicAuth struct {
	Store   loginStore
	RunFunc func(response.FormatWriter, *http.Request, *model.User)
}

// ServeHTTP satisfies the Handler interface.
func (b *BasicAuth) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var fw response.FormatWriter

	// Prefer application/json by default.
	accept := r.Header.Get("Accept")
	switch {
	case strings.Contains(accept, "application/xml"):
		fw = response.NewXMLWriter(w, time.Now(), "preauth", "preauth")
		fw.Header().Set("Content-Type", "application/xml")
	default:
		fw = response.NewJSONWriter(w, time.Now(), "preauth", "preauth")
		fw.Header().Set("Content-Type", "application/json")
	}

	b.AuthRun(fw, r)
	logHandlerFunc(fw, r)
}

// AuthRun authenticates by login/pass and runs RunFunc if successful.
func (b *BasicAuth) AuthRun(fw response.FormatWriter, r *http.Request) {
	login, pass, ok := r.BasicAuth()
	if !ok {
		response.Unauthorized(fw, r)
		return
	}

	client, err := b.Store.ReadUserByLogin(login)
	if err != nil {
		log.Printf("Login failure, GetUserByLogin: %s - %v", login, err)
		response.Unauthorized(fw, r)
		return
	}

	if err = bcrypt.CompareHashAndPassword([]byte(client.BcryptHash),
		[]byte(pass)); err != nil {
		log.Printf("Login failure, CompareHashAndPassword: %s - %v", login, err)
		response.Unauthorized(fw, r)
	} else {
		fw.SetUser("basic:" + client.Login)
		fw.SetUserType(client.Role)
		b.RunFunc(fw, r, client)
	}
}
