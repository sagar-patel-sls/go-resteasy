// +build !integration

package model

import (
	"errors"
	"fmt"
	"reflect"
	"testing"
)

func TestValidateJob(t *testing.T) {
	t.Parallel()

	tests := []struct {
		inp Job
		res error
	}{
		{Job{UserID: "job1"}, nil},
		{Job{}, errors.New(jobReqUserID)},
	}

	for _, test := range tests {
		lTest := test

		t.Run(fmt.Sprintf("Can validate %+v", lTest), func(t *testing.T) {
			t.Parallel()

			err := lTest.inp.Validate()
			t.Logf("err: %#v", err)
			if !reflect.DeepEqual(lTest.res, err) {
				t.Fatalf("Expected, actual: %#v, %#v", lTest.res, err)
			}
		})
	}
}
