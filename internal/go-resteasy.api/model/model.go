package model

// AdminRole is the administrator user role.
const AdminRole = "admin"

// Link models relations and locations in resources.
type Link struct {
	Rel  string `json:"rel" xml:"rel,attr"`
	Href string `json:"href" xml:"href,attr"`
}
