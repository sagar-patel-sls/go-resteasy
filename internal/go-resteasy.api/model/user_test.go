// +build !integration

package model

import (
	"errors"
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/bconway/go-resteasy/pkg/test/random"
)

func TestValidateUser(t *testing.T) {
	t.Parallel()

	pass := random.String(10)

	tests := []struct {
		inp User
		res error
	}{
		{User{Role: "user", FirstName: "First", LastName: "Last",
			Email: "email@example.com", Login: "flast",
			BcryptHash: random.String(10)}, nil},
		{User{Role: "user", FirstName: "First", LastName: "Last",
			Email: "email@example.com", Login: "flast", Password: &pass}, nil},
		{User{FirstName: "First", LastName: "Last", Email: "email@example.com",
			Login: "flast", BcryptHash: random.String(10)},
			errors.New(userReqUserTypeID)},
		{User{Role: "user", LastName: "Last", Email: "email@example.com",
			Login: "flast", BcryptHash: random.String(10)},
			errors.New(userReqFirstName)},
		{User{Role: "user", FirstName: "First", Email: "email@example.com",
			Login: "flast", BcryptHash: random.String(10)},
			errors.New(userReqLastName)},
		{User{Role: "user", FirstName: "First", LastName: "Last",
			Login: "flast", BcryptHash: random.String(10)},
			errors.New(userReqEmail)},
		{User{Role: "user", FirstName: "First", LastName: "Last",
			Email: "email@example.com", BcryptHash: random.String(10)},
			errors.New(userReqLogin)},
		{User{Role: "user", FirstName: "First", LastName: "Last",
			Email: "email@example.com", Login: "flast"},
			errors.New(userReqPassword)},
	}

	for _, test := range tests {
		lTest := test

		t.Run(fmt.Sprintf("Can validate %+v", lTest), func(t *testing.T) {
			t.Parallel()

			err := lTest.inp.Validate()
			t.Logf("err: %#v", err)
			if !reflect.DeepEqual(lTest.res, err) {
				t.Fatalf("Expected, actual: %#v, %#v", lTest.res, err)
			}
		})
	}
}
