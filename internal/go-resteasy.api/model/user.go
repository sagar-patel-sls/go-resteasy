package model

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"io"
)

// Error strings used for validation.
const (
	userReqUserTypeID = "required field missing or invalid: role"
	userReqFirstName  = "required field missing: firstName"
	userReqLastName   = "required field missing: lastName"
	userReqEmail      = "required field missing: email"
	userReqLogin      = "required field missing: login"
	userReqPassword   = "required field missing: password"
)

// User models users as defined in table users.
// Status defaults to "active". Password is used for client input.
// BcryptHash is never exported.
type User struct {
	ID         string       `json:"id" xml:"ID"`
	Role       string       `json:"role" xml:"Role"`
	FirstName  string       `json:"firstName" xml:"FirstName"`
	LastName   string       `json:"lastName" xml:"LastName"`
	Email      string       `json:"email" xml:"Email"`
	Status     string       `json:"status" xml:"Status"`
	Login      string       `json:"login" xml:"Login"`
	Password   *string      `json:"password,omitempty" xml:"Password,omitempty"`
	BcryptHash string       `json:"-" xml:"-"`
	PhoneNum   *string      `json:"phoneNum,omitempty" xml:"PhoneNum,omitempty"`
	Address    *string      `json:"address,omitempty" xml:"Address,omitempty"`
	City       *string      `json:"city,omitempty" xml:"City,omitempty"`
	State      *string      `json:"state,omitempty" xml:"State,omitempty"`
	Zip        *string      `json:"zip,omitempty" xml:"Aip,omitempty"`
	AccessKeys []*AccessKey `json:"accessKeys,omitempty" xml:"AccessKeys>AccessKey,omitempty"`
	Links      []Link       `json:"links" xml:"Links>Link"`
}

// DecodeUser decodes and replaces/patches a passed User based on contentType.
func DecodeUser(contentType string, body io.Reader, user *User) error {
	var err error

	// Prefer application/json by default.
	switch contentType {
	case "application/xml":
		decoder := xml.NewDecoder(body)
		err = decoder.Decode(user)
	default:
		decoder := json.NewDecoder(body)
		err = decoder.Decode(user)
	}
	return err
}

// Validate sets an error in the event that a User is not valid.
func (u *User) Validate() error {
	switch {
	case u.Role == "":
		return errors.New(userReqUserTypeID)
	case u.FirstName == "":
		return errors.New(userReqFirstName)
	case u.LastName == "":
		return errors.New(userReqLastName)
	case u.Email == "":
		return errors.New(userReqEmail)
	case u.Login == "":
		return errors.New(userReqLogin)
	case u.Password == nil && u.BcryptHash == "":
		return errors.New(userReqPassword)
	}
	return nil
}

// PopulateLinks sets the Links slice with a self rel and href URL.
func (u *User) PopulateLinks() {
	u.Links = []Link{{Rel: "self", Href: "/users/" + u.ID}}
}
