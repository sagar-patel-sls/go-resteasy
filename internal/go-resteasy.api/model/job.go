package model

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"io"
	"time"
)

// Error strings used for validation.
const (
	jobReqUserID = "required field missing or invalid: userId"
)

// Job models jobs as defined in table jobs.
// Status defaults to "ready".
type Job struct {
	ID          string     `json:"id" xml:"ID"`
	UserID      string     `json:"userId" xml:"UserID"`
	Status      string     `json:"status" xml:"Status"`
	StartedAt   *time.Time `json:"startedAt,omitempty" xml:"StartedAt,omitempty"`
	CompletedAt *time.Time `json:"completedAt,omitempty" xml:"CompletedAt,omitempty"`
	Links       []Link     `json:"links" xml:"Links>Link"`
}

// DecodeJob decodes and replaces/patches a passed Job based on contentType.
func DecodeJob(contentType string, body io.Reader, job *Job) error {
	var err error

	// Prefer application/json by default.
	switch contentType {
	case "application/xml":
		decoder := xml.NewDecoder(body)
		err = decoder.Decode(job)
	default:
		decoder := json.NewDecoder(body)
		err = decoder.Decode(job)
	}
	return err
}

// Validate sets an error in the event that a Job is not valid.
func (j *Job) Validate() error {
	if j.UserID == "" {
		return errors.New(jobReqUserID)
	}
	return nil
}

// PopulateLinks sets the Links slice with a self rel and href URL.
func (j *Job) PopulateLinks() {
	j.Links = []Link{{Rel: "self", Href: "/jobs/" + j.ID}}
}
