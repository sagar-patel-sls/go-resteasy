package model

import (
	"time"
)

// AccessKey models API keys as defined in table access_keys.
// Key defaults to gen_random_uuid().
type AccessKey struct {
	Key       string    `json:"key" xml:"Key"`
	ExpiresAt time.Time `json:"expiresAt" xml:"ExpiresAt"`
}
