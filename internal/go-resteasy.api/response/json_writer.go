package response

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

// JSONWriter is a FormatWriter that handles encoding JSON data and errors. It
// also satisfies the http.ResponseWriter interface.
type JSONWriter struct {
	http.ResponseWriter
	statusCode int
	startTime  time.Time
	user       string
	userType   string
}

// NewJSONWriter builds a JSONWriter and returns a pointer to it.
func NewJSONWriter(w http.ResponseWriter, startTime time.Time, user,
	userType string) *JSONWriter {
	return &JSONWriter{
		ResponseWriter: w,
		startTime:      startTime,
		user:           user,
		userType:       userType,
	}
}

// WriteHeader sets the statusCode and passes through to http.ResponseWriter's
// WriteHeader.
func (jw *JSONWriter) WriteHeader(i int) {
	jw.statusCode = i
	jw.ResponseWriter.WriteHeader(i)
}

// StatusCode returns the JSONWriter's statusCode.
func (jw *JSONWriter) StatusCode() int {
	return jw.statusCode
}

// StartTime returns the JSONWriter's startTime.
func (jw *JSONWriter) StartTime() time.Time {
	return jw.startTime
}

// SetUser sets the JSONWriter's user.
func (jw *JSONWriter) SetUser(u string) {
	jw.user = u
}

// User returns the JSONWriter's user.
func (jw *JSONWriter) User() string {
	return jw.user
}

// SetUserType sets the JSONWriter's userType.
func (jw *JSONWriter) SetUserType(ut string) {
	jw.userType = ut
}

// UserType returns the JSONWriter's userType.
func (jw *JSONWriter) UserType() string {
	return jw.userType
}

// WriteFormat accepts an interface and writes JSON via the JSONWriter. Errors
// on marshalling are redirected to WriteError.
func (jw *JSONWriter) WriteFormat(status int, i interface{}) {
	if data, err := json.Marshal(i); err != nil {
		jw.WriteError(http.StatusInternalServerError, err)
	} else {
		jw.WriteHeader(status)
		if _, err := jw.Write(data); err != nil {
			log.Printf("WriteFormat jw.Write err: %#v", err)
		}
	}
}

// WriteError sets a failure-relevant Header and writes a {"error":"..."} Body.
func (jw *JSONWriter) WriteError(status int, err error) {
	jw.WriteFormat(status, map[string]string{"error": err.Error()})
}
