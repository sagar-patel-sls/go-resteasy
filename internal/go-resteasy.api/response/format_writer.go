package response

import (
	"net/http"
	"time"
)

// A FormatWriter is used to format JSON, XML, etc, and satisfies the
// http.ResponseWriter interface.
type FormatWriter interface {
	http.ResponseWriter

	StatusCode() int
	StartTime() time.Time
	SetUser(u string)
	User() string
	SetUserType(ut string)
	UserType() string

	WriteFormat(status int, i interface{})
	WriteError(status int, err error)
}
