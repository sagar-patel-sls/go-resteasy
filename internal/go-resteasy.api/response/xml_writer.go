package response

import (
	"encoding/xml"
	"log"
	"net/http"
	"time"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/model"
)

// XMLWriter is a FormatWriter that handles encoding XML data and errors. It
// also satisfies the http.ResponseWriter interface.
type XMLWriter struct {
	http.ResponseWriter
	statusCode int
	startTime  time.Time
	user       string
	userType   string
}

// NewXMLWriter builds an XMLWriter and returns a pointer to it.
func NewXMLWriter(w http.ResponseWriter, startTime time.Time, user,
	userType string) *XMLWriter {
	return &XMLWriter{
		ResponseWriter: w,
		startTime:      startTime,
		user:           user,
		userType:       userType,
	}
}

// WriteHeader sets the statusCode and passes through to http.ResponseWriter's
// WriteHeader.
func (xw *XMLWriter) WriteHeader(i int) {
	xw.statusCode = i
	xw.ResponseWriter.WriteHeader(i)
}

// StatusCode returns the XMLWriter's statusCode.
func (xw *XMLWriter) StatusCode() int {
	return xw.statusCode
}

// StartTime returns the XMLWriter's startTime.
func (xw *XMLWriter) StartTime() time.Time {
	return xw.startTime
}

// SetUser sets the XMLWriter's user.
func (xw *XMLWriter) SetUser(u string) {
	xw.user = u
}

// User returns the XMLWriter's user.
func (xw *XMLWriter) User() string {
	return xw.user
}

// SetUserType sets the XMLWriter's userType.
func (xw *XMLWriter) SetUserType(ut string) {
	xw.userType = ut
}

// UserType returns the XMLWriter's userType.
func (xw *XMLWriter) UserType() string {
	return xw.userType
}

// WriteFormat accepts an interface and writes XML via the XMLWriter.
// Errors on marshalling are redirected to WriteError.
func (xw *XMLWriter) WriteFormat(status int, i interface{}) {
	var m interface{}

	switch v := i.(type) {
	case *model.User:
		m = xmlUser{U: v}
	case []*model.User:
		m = xmlUsers{Us: v}
	case *model.Job:
		m = xmlJob{J: v}
	case []*model.Job:
		m = xmlJobs{Js: v}
	default:
		m = i
	}

	if data, err := xml.Marshal(m); err != nil {
		xw.WriteError(http.StatusInternalServerError, err)
	} else {
		xw.WriteHeader(status)
		if _, err := xw.Write(data); err != nil {
			log.Printf("WriteFormat xw.Write err: %#v", err)
		}
	}
}

// WriteError sets a failure-relevant Header and writes a <error>...</error> Body.
func (xw *XMLWriter) WriteError(status int, err error) {
	xw.WriteFormat(status, xmlError{Error: err.Error()})
}

// xmlUser carries a User and the response wrapper.
type xmlUser struct {
	XMLName xml.Name    `xml:"Response"`
	U       *model.User `xml:"User"`
}

// xmlUsers carries a slice of Users and the response wrapper.
type xmlUsers struct {
	XMLName xml.Name      `xml:"Response"`
	Us      []*model.User `xml:"Users>User"`
}

// xmlJob carries a Job and the response wrapper.
type xmlJob struct {
	XMLName xml.Name   `xml:"Response"`
	J       *model.Job `xml:"Job"`
}

// xmlJobs carries a slice of Jobs and the response wrapper.
type xmlJobs struct {
	XMLName xml.Name     `xml:"Response"`
	Js      []*model.Job `xml:"Jobs>Job"`
}

// xmlError carries an error and the response wrapper.
type xmlError struct {
	XMLName xml.Name `xml:"Response"`
	Error   string   `xml:"Error"`
}
