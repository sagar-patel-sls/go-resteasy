package response

import (
	"errors"
	"net/http"
)

// Unauthorized is a convenience function for 401 Unauthorized.
func Unauthorized(fw FormatWriter, r *http.Request) {
	fw.WriteError(http.StatusUnauthorized, errors.New("unauthorized"))
}

// Forbidden is a convenience function for 403 Forbidden.
func Forbidden(fw FormatWriter, r *http.Request) {
	fw.WriteError(http.StatusForbidden, errors.New("forbidden"))
}

// NotFound is a convenience function for 404 Not Found.
func NotFound(fw FormatWriter, r *http.Request) {
	fw.WriteError(http.StatusNotFound, errors.New("not found"))
}

// MethodNotAllowed is a convenience function for 405 Method Not Allowed.
func MethodNotAllowed(fw FormatWriter, r *http.Request) {
	fw.WriteError(http.StatusMethodNotAllowed, errors.New("method not allowed"))
}
