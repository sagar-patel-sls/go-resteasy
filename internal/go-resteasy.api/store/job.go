package store

import (
	"database/sql"
	"log"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/model"
)

const createJob = `
INSERT INTO jobs (user_id, started_at, completed_at)
VALUES ($1, $2, $3)
RETURNING id
`

// CreateJob validates and creates a new Job.
func (s *Store) CreateJob(client *model.User, j *model.Job) error {
	// Users are not allowed to set another UserId.
	if j.UserID == "" || client.Role == "user" {
		j.UserID = client.ID
	}

	if err := j.Validate(); err != nil {
		return err
	}

	if err := s.DB.QueryRow(createJob, j.UserID, j.StartedAt,
		j.CompletedAt).Scan(&j.ID); err != nil {
		return err
	}

	j.Status = "ready"
	j.PopulateLinks()
	return nil
}

const readJob = `
SELECT id, user_id, status, started_at, completed_at
FROM jobs
WHERE id = $1
`

// ReadJob returns a single Job by ID.
func (s *Store) ReadJob(client *model.User, jobID string) (*model.Job, error) {
	var row *sql.Row

	if client.Role == model.AdminRole {
		row = s.DB.QueryRow(readJob, jobID)
	} else {
		row = s.DB.QueryRow(readJob+" AND user_id = $2", jobID, client.ID)
	}

	var j model.Job
	if err := row.Scan(&j.ID, &j.UserID, &j.Status, &j.StartedAt,
		&j.CompletedAt); err != nil {
		return nil, err
	}

	j.PopulateLinks()
	return &j, nil
}

const updateJob = `
UPDATE jobs
SET user_id = $1, started_at = $2, completed_at = $3
WHERE id = $4
`

// UpdateJob validates and updates a job in-place with a full resource
// representation.
func (s *Store) UpdateJob(client *model.User, j *model.Job) error {
	// Users are not allowed to set another UserId.
	if j.UserID == "" || client.Role == "user" {
		j.UserID = client.ID
	}

	if err := j.Validate(); err != nil {
		return err
	}

	if _, err := s.DB.Exec(updateJob, j.UserID, j.StartedAt, j.CompletedAt,
		j.ID); err != nil {
		return err
	}

	j.PopulateLinks()
	return nil
}

const deleteJob = `
UPDATE jobs
SET status = 'deleted'
WHERE id = $1
`

// DeleteJob deletes (hides) a job by setting its status to "deleted".
func (s *Store) DeleteJob(jobID string) error {
	_, err := s.DB.Exec(deleteJob, jobID)
	return err
}

const listJobs = `
SELECT id, user_id, status, started_at, completed_at
FROM jobs
WHERE status <> 'deleted'
`

// ListJobs returns a slice of all Jobs from the data store.
func (s *Store) ListJobs(client *model.User) ([]*model.Job, error) {
	var rows *sql.Rows
	var err error
	var jobs []*model.Job

	if client.Role == model.AdminRole {
		rows, err = s.DB.Query(listJobs)
	} else {
		rows, err = s.DB.Query(listJobs+" AND user_id = $1", client.ID)
	}
	if err != nil {
		return nil, err
	}
	defer func() {
		if err = rows.Close(); err != nil {
			log.Printf("ListJobs rows.Close err: %#v", err)
		}
	}()

	for rows.Next() {
		var j model.Job
		if err = rows.Scan(&j.ID, &j.UserID, &j.Status, &j.StartedAt,
			&j.CompletedAt); err != nil {
			return nil, err
		}
		j.PopulateLinks()
		jobs = append(jobs, &j)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}
	return jobs, nil
}
