package store

import (
	"database/sql"
	"log"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/model"
	"golang.org/x/crypto/bcrypt"
)

const createUser = `
INSERT INTO users (role, first_name, last_name, email, login, bcrypt_hash,
phone_num, address, city, state, zip)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
RETURNING id
`

// CreateUser validates and creates a new User.
func (s *Store) CreateUser(u *model.User) error {
	if err := u.Validate(); err != nil {
		return err
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(*u.Password),
		bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.Password = nil

	if err = s.DB.QueryRow(createUser, u.Role, u.FirstName, u.LastName, u.Email,
		u.Login, hash, u.PhoneNum, u.Address, u.City, u.State, u.Zip).Scan(
		&u.ID); err != nil {
		return err
	}

	u.Status = "active"
	u.PopulateLinks()
	return nil
}

const readUser = `
SELECT id, role, first_name, last_name, email, status, login, bcrypt_hash,
phone_num, address, city, state, zip
FROM users
WHERE id = $1
`

// ReadUser returns a single User by ID.
func (s *Store) ReadUser(client *model.User, userID string) (*model.User,
	error) {
	var row *sql.Row

	if client.Role == model.AdminRole {
		row = s.DB.QueryRow(readUser, userID)
	} else {
		row = s.DB.QueryRow(readUser+" AND id = $2", userID, client.ID)
	}

	var u model.User
	if err := row.Scan(&u.ID, &u.Role, &u.FirstName, &u.LastName, &u.Email,
		&u.Status, &u.Login, &u.BcryptHash, &u.PhoneNum, &u.Address, &u.City,
		&u.State, &u.Zip); err != nil {
		return nil, err
	}

	u.PopulateLinks()
	return &u, nil
}

const readUserByLogin = `
SELECT id, role, first_name, last_name, email, status, login, bcrypt_hash,
phone_num, address, city, state, zip
FROM users
WHERE login = $1
AND status = 'active'
`

// ReadUserByLogin returns an active User by Login.
func (s *Store) ReadUserByLogin(login string) (*model.User, error) {
	var u model.User

	if err := s.DB.QueryRow(readUserByLogin, login).Scan(&u.ID, &u.Role,
		&u.FirstName, &u.LastName, &u.Email, &u.Status, &u.Login, &u.BcryptHash,
		&u.PhoneNum, &u.Address, &u.City, &u.State, &u.Zip); err != nil {
		return nil, err
	}

	u.PopulateLinks()
	return &u, nil
}

const readUserByKey = `
SELECT u.id, u.role, u.first_name, u.last_name, u.email, u.status, u.login,
u.bcrypt_hash, u.phone_num, u.address, u.city, u.state, u.zip
FROM users u, access_keys ak
WHERE u.id = ak.user_id
AND ak.key = $1
AND ak.expires_at > now()
AND u.status = 'active'
`

// ReadUserByKey returns an active User by AccessKey key.
func (s *Store) ReadUserByKey(key string) (*model.User, error) {
	var u model.User

	if err := s.DB.QueryRow(readUserByKey, key).Scan(&u.ID, &u.Role,
		&u.FirstName, &u.LastName, &u.Email, &u.Status, &u.Login, &u.BcryptHash,
		&u.PhoneNum, &u.Address, &u.City, &u.State, &u.Zip); err != nil {
		return nil, err
	}

	u.PopulateLinks()
	return &u, nil
}

const updateUser = `
UPDATE users SET role = $1, first_name = $2, last_name = $3, email = $4,
status = $5, login = $6, bcrypt_hash = $7, phone_num = $8, address = $9,
city = $10, state = $11, zip = $12
WHERE id = $13
`

// UpdateUser validates and updates a user in-place with a full resource
// representation.
func (s *Store) UpdateUser(client *model.User, u *model.User) error {
	// Users are not allowed to set another UserType.
	if u.Role == "" || client.Role != model.AdminRole {
		u.Role = client.Role
	}

	if err := u.Validate(); err != nil {
		return err
	}

	if u.Password != nil {
		hash, err := bcrypt.GenerateFromPassword([]byte(*u.Password),
			bcrypt.DefaultCost)
		if err != nil {
			return err
		}

		u.BcryptHash = string(hash)
		u.Password = nil
	}

	if _, err := s.DB.Exec(updateUser, u.Role, u.FirstName, u.LastName,
		u.Email, u.Status, u.Login, u.BcryptHash, u.PhoneNum, u.Address, u.City,
		u.State, u.Zip, u.ID); err != nil {
		return err
	}

	u.PopulateLinks()
	return nil
}

const deleteUser = `
UPDATE users
SET status = 'deleted'
WHERE id = $1
`

// DeleteUser deletes (hides) a user by setting their status to "deleted".
func (s *Store) DeleteUser(userID string) error {
	_, err := s.DB.Exec(deleteUser, userID)
	return err
}

const listUsers = `
SELECT id, role, first_name, last_name, email, status, login, phone_num,
address, city, state, zip
FROM users
WHERE status <> 'deleted'
`

// ListUsers returns a slice of all Users from the data store.
func (s *Store) ListUsers(client *model.User) ([]*model.User, error) {
	var rows *sql.Rows
	var err error
	var users []*model.User

	if client.Role == model.AdminRole {
		rows, err = s.DB.Query(listUsers)
	} else {
		rows, err = s.DB.Query(listUsers+" AND id = $1", client.ID)
	}
	if err != nil {
		return nil, err
	}
	defer func() {
		if err = rows.Close(); err != nil {
			log.Printf("ListUsers rows.Close err: %#v", err)
		}
	}()

	for rows.Next() {
		var u model.User
		if err = rows.Scan(&u.ID, &u.Role, &u.FirstName, &u.LastName,
			&u.Email, &u.Status, &u.Login, &u.PhoneNum, &u.Address, &u.City,
			&u.State, &u.Zip); err != nil {
			return nil, err
		}
		u.PopulateLinks()
		users = append(users, &u)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}
	return users, nil
}
