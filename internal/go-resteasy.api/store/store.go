package store

import (
	"database/sql"
)

// Store contains database connections and methods for interacting with them.
type Store struct {
	DB *sql.DB
}
