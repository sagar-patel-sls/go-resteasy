package store

import (
	"time"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/model"
)

const createAccessKey = `
INSERT INTO access_keys (user_id, expires_at)
VALUES ($1, $2)
RETURNING key
`

// CreateAccessKey creates a new AccessKey for a known user ID.
func (s *Store) CreateAccessKey(userID string) (*model.AccessKey, error) {
	ak := &model.AccessKey{ExpiresAt: time.Now().Add(24 * time.Hour)}

	if err := s.DB.QueryRow(createAccessKey, userID, ak.ExpiresAt).Scan(
		&ak.Key); err != nil {
		return nil, err
	}
	return ak, nil
}

const listAccessKeys = `
SELECT key, expires_at
FROM access_keys
WHERE user_id = $1
AND expires_at > now()
ORDER BY expires_at DESC
`

// ListAccessKeys returns the most recent AccessKeys by user ID.
func (s *Store) ListAccessKeys(userID string) ([]*model.AccessKey, error) {
	var keys []*model.AccessKey

	rows, err := s.DB.Query(listAccessKeys, userID)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		ak := &model.AccessKey{}
		if err = rows.Scan(&ak.Key, &ak.ExpiresAt); err != nil {
			return nil, err
		}
		keys = append(keys, ak)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}
	return keys, nil
}

// RefreshAccessKeys refreshes and builds the AccessKeys slice.
func (s *Store) RefreshAccessKeys(userID string) ([]*model.AccessKey, error) {
	keys, err := s.ListAccessKeys(userID)
	if err != nil {
		return nil, err
	}

	if len(keys) == 0 || keys[0].ExpiresAt.Before(time.Now().Add(time.Hour)) {
		key, err := s.CreateAccessKey(userID)
		if err != nil {
			return nil, err
		}
		keys = append([]*model.AccessKey{key}, keys...)
	}
	return keys, nil
}
