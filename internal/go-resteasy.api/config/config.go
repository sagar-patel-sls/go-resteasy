package config

import "gitlab.com/bconway/go-resteasy/pkg/config"

const pref = "RE_"

// Config holds settings used by service implementations.
type Config struct {
	PgURI string
}

// New instantiates a service Config, parses the environment, and returns it.
func New() *Config {
	return &Config{
		PgURI: config.String(pref+"PGURI",
			"postgres://postgres:postgres@localhost/re"),
	}
}
