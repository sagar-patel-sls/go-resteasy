package controller

import (
	"net/http"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/middleware"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/response"
)

// RootHandler returns a Handler that processes root requests.
func RootHandler() *middleware.NoAuth {
	return &middleware.NoAuth{
		RunFunc: func(fw response.FormatWriter, r *http.Request) {
			response.NotFound(fw, r)
		},
	}
}
