package controller

import (
	"database/sql"
	"net/http"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/middleware"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/model"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/response"
)

// userStore defines the methods used by a User controller.
type userStore interface {
	ListUsers(client *model.User) ([]*model.User, error)
	CreateUser(user *model.User) error
	ReadUser(client *model.User, userID string) (*model.User, error)
	UpdateUser(client *model.User, user *model.User) error
	DeleteUser(userID string) error
	// Satisfy loginStore.
	ReadUserByLogin(login string) (*model.User, error)
	ReadUserByKey(key string) (*model.User, error)
}

// User controller provides methods for Users.
type User struct {
	Store userStore
}

// UserHandler returns a Handler that processes User requests.
func (u *User) UserHandler() *middleware.KeyAuth {
	return &middleware.KeyAuth{
		Store: u.Store,
		RunFunc: func(fw response.FormatWriter, r *http.Request,
			client *model.User) {
			switch {
			case r.Method == http.MethodGet && r.URL.Path == "":
				u.listUsers(fw, client)
			case r.Method == http.MethodPost && r.URL.Path == "":
				u.createUser(fw, r, client)
			case r.Method == http.MethodGet:
				u.readUser(fw, r, client, r.URL.Path)
			case r.Method == http.MethodPut:
				u.updateUser(fw, r, client, r.URL.Path)
			case r.Method == http.MethodPatch:
				u.patchUser(fw, r, client, r.URL.Path)
			case r.Method == http.MethodDelete:
				u.deleteUser(fw, r, client, r.URL.Path)
			default:
				response.MethodNotAllowed(fw, r)
			}
		},
	}
}

// listUsers passes through the request to the store's ListUsers.
func (u *User) listUsers(fw response.FormatWriter, client *model.User) {
	if users, err := u.Store.ListUsers(client); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
	} else {
		fw.WriteFormat(http.StatusOK, users)
	}
}

// createUser passes through the request to the store's CreateUser.
func (u *User) createUser(fw response.FormatWriter, r *http.Request,
	client *model.User) {
	// Only admins are allowed to create users.
	if client.Role != model.AdminRole {
		response.Forbidden(fw, r)
		return
	}

	var user model.User
	if err := model.DecodeUser(r.Header.Get("Content-Type"), r.Body,
		&user); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
		return
	}

	if err := u.Store.CreateUser(&user); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
	} else {
		fw.WriteFormat(http.StatusCreated, &user)
	}
}

// readUser passes through the request to the store's ReadUser.
func (u *User) readUser(fw response.FormatWriter, r *http.Request,
	client *model.User, id string) {
	if user, err := u.Store.ReadUser(client, id); err != nil {
		if err == sql.ErrNoRows {
			response.NotFound(fw, r)
		} else {
			fw.WriteError(http.StatusBadRequest, err)
		}
	} else {
		fw.WriteFormat(http.StatusOK, user)
	}
}

// UpdateUser passes through the request to the store's UpdateUser.
func (u *User) updateUser(fw response.FormatWriter, r *http.Request,
	client *model.User, id string) {
	ouser, err := u.Store.ReadUser(client, id)
	if err != nil {
		if err == sql.ErrNoRows {
			response.NotFound(fw, r)
		} else {
			fw.WriteError(http.StatusBadRequest, err)
		}
		return
	}

	var nuser model.User
	if err = model.DecodeUser(r.Header.Get("Content-Type"), r.Body,
		&nuser); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
		return
	}

	// ID is not changed via Update(), copy it over.
	nuser.ID = ouser.ID

	if err = u.Store.UpdateUser(client, &nuser); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
	} else {
		fw.WriteFormat(http.StatusOK, &nuser)
	}
}

// patchUser patches the User's attributes and passes it through to the store's
// UpdateUser.
func (u *User) patchUser(fw response.FormatWriter, r *http.Request,
	client *model.User, id string) {
	user, err := u.Store.ReadUser(client, id)
	if err != nil {
		if err == sql.ErrNoRows {
			response.NotFound(fw, r)
		} else {
			fw.WriteError(http.StatusBadRequest, err)
		}
		return
	}
	uid := user.ID

	if err = model.DecodeUser(r.Header.Get("Content-Type"), r.Body,
		user); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
		return
	}

	// ID is not changed via Update(), copy it over.
	user.ID = uid

	if err = u.Store.UpdateUser(client, user); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
	} else {
		fw.WriteFormat(http.StatusOK, user)
	}
}

// deleteUser looks up a User by ID and then deletes (hides) it.
func (u *User) deleteUser(fw response.FormatWriter, r *http.Request,
	client *model.User, id string) {
	// Only admins are allowed to delete users.
	if client.Role != model.AdminRole {
		response.Forbidden(fw, r)
		return
	}

	user, err := u.Store.ReadUser(client, id)
	if err != nil {
		if err == sql.ErrNoRows {
			response.NotFound(fw, r)
		} else {
			fw.WriteError(http.StatusBadRequest, err)
		}
		return
	}

	if err = u.Store.DeleteUser(user.ID); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
	} else {
		fw.WriteHeader(http.StatusNoContent)
	}
}
