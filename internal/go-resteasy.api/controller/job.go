package controller

import (
	"database/sql"
	"net/http"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/middleware"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/model"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/response"
)

// jobStore defines the methods used by a Job controller.
type jobStore interface {
	ListJobs(client *model.User) ([]*model.Job, error)
	CreateJob(client *model.User, job *model.Job) error
	ReadJob(client *model.User, jobID string) (*model.Job, error)
	UpdateJob(client *model.User, job *model.Job) error
	DeleteJob(jobID string) error
	// Satisfy loginStore.
	ReadUserByLogin(login string) (*model.User, error)
	ReadUserByKey(key string) (*model.User, error)
}

// Job controller provides methods for Jobs.
type Job struct {
	Store jobStore
}

// JobHandler returns a Handler that processes Job requests.
func (j *Job) JobHandler() *middleware.KeyAuth {
	return &middleware.KeyAuth{
		Store: j.Store,
		RunFunc: func(fw response.FormatWriter, r *http.Request,
			client *model.User) {
			switch {
			case r.Method == http.MethodGet && r.URL.Path == "":
				j.listJobs(fw, client)
			case r.Method == http.MethodPost && r.URL.Path == "":
				j.createJob(fw, r, client)
			case r.Method == http.MethodGet:
				j.readJob(fw, r, client, r.URL.Path)
			case r.Method == http.MethodPut:
				j.updateJob(fw, r, client, r.URL.Path)
			case r.Method == http.MethodPatch:
				j.patchJob(fw, r, client, r.URL.Path)
			case r.Method == http.MethodDelete:
				j.deleteJob(fw, r, client, r.URL.Path)
			default:
				response.MethodNotAllowed(fw, r)
			}
		},
	}
}

// listJobs passes through the request to the store's ListJobs.
func (j *Job) listJobs(fw response.FormatWriter, client *model.User) {
	if jobs, err := j.Store.ListJobs(client); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
	} else {
		fw.WriteFormat(http.StatusOK, jobs)
	}
}

// createJob passes through the request to the store's CreateJob.
func (j *Job) createJob(fw response.FormatWriter, r *http.Request,
	client *model.User) {
	var job model.Job
	if err := model.DecodeJob(r.Header.Get("Content-Type"), r.Body,
		&job); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
		return
	}

	if err := j.Store.CreateJob(client, &job); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
	} else {
		fw.WriteFormat(http.StatusCreated, &job)
	}
}

// readJob passes through the request to the store's ReadJob.
func (j *Job) readJob(fw response.FormatWriter, r *http.Request,
	client *model.User, id string) {
	if job, err := j.Store.ReadJob(client, id); err != nil {
		if err == sql.ErrNoRows {
			response.NotFound(fw, r)
		} else {
			fw.WriteError(http.StatusBadRequest, err)
		}
	} else {
		fw.WriteFormat(http.StatusOK, job)
	}
}

// updateJob passes through the request to the store's UpdateJob.
func (j *Job) updateJob(fw response.FormatWriter, r *http.Request,
	client *model.User, id string) {
	ojob, err := j.Store.ReadJob(client, id)
	if err != nil {
		if err == sql.ErrNoRows {
			response.NotFound(fw, r)
		} else {
			fw.WriteError(http.StatusBadRequest, err)
		}
		return
	}

	var njob model.Job
	if err = model.DecodeJob(r.Header.Get("Content-Type"), r.Body,
		&njob); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
		return
	}

	// ID is not changed via Update(), copy it over.
	njob.ID = ojob.ID

	if err = j.Store.UpdateJob(client, &njob); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
	} else {
		fw.WriteFormat(http.StatusOK, &njob)
	}
}

// patchJob patches the Job's attributes and passes it through to the store's
// UpdateJob.
func (j *Job) patchJob(fw response.FormatWriter, r *http.Request,
	client *model.User, id string) {
	job, err := j.Store.ReadJob(client, id)
	if err != nil {
		if err == sql.ErrNoRows {
			response.NotFound(fw, r)
		} else {
			fw.WriteError(http.StatusBadRequest, err)
		}
		return
	}

	jid := job.ID

	if err = model.DecodeJob(r.Header.Get("Content-Type"), r.Body,
		job); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
		return
	}

	// ID is not changed via Update(), copy it over.
	job.ID = jid

	if err = j.Store.UpdateJob(client, job); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
	} else {
		fw.WriteFormat(http.StatusOK, job)
	}
}

// deleteJob looks up a Job by ID and then deletes (hides) it.
func (j *Job) deleteJob(fw response.FormatWriter, r *http.Request,
	client *model.User, id string) {
	job, err := j.Store.ReadJob(client, id)
	if err != nil {
		if err == sql.ErrNoRows {
			response.NotFound(fw, r)
		} else {
			fw.WriteError(http.StatusBadRequest, err)
		}
		return
	}

	if err = j.Store.DeleteJob(job.ID); err != nil {
		fw.WriteError(http.StatusBadRequest, err)
	} else {
		fw.WriteHeader(http.StatusNoContent)
	}
}
