package controller

import (
	"net/http"

	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/middleware"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/model"
	"gitlab.com/bconway/go-resteasy/internal/go-resteasy.api/response"
)

// authStore defines the methods used by an Auth controller.
type authStore interface {
	RefreshAccessKeys(userID string) ([]*model.AccessKey, error)
	// Satisfy loginStore.
	ReadUserByLogin(login string) (*model.User, error)
	ReadUserByKey(key string) (*model.User, error)
}

// Auth controller provides methods for authentication.
type Auth struct {
	Store authStore
}

// AuthHandler returns a Handler that processes authentication requests.
func (a *Auth) AuthHandler() *middleware.BasicAuth {
	return &middleware.BasicAuth{
		Store: a.Store,
		RunFunc: func(fw response.FormatWriter, r *http.Request,
			client *model.User) {
			switch {
			case r.Method == http.MethodGet && r.URL.Path == "":
				a.handleGetUserAuth(fw, client)
			default:
				response.MethodNotAllowed(fw, r)
			}
		},
	}
}

// handleGetUserAuth populates Access Keys and sends the User.
func (a *Auth) handleGetUserAuth(fw response.FormatWriter, client *model.User) {
	if keys, err := a.Store.RefreshAccessKeys(client.ID); err != nil {
		fw.WriteError(http.StatusInternalServerError, err)
	} else {
		client.AccessKeys = keys
		fw.WriteFormat(http.StatusOK, client)
	}
}
