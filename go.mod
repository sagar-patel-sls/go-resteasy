module gitlab.com/bconway/go-resteasy

go 1.12

require (
	github.com/jackc/pgx v3.5.0+incompatible
	github.com/pkg/errors v0.8.1 // indirect
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
)
