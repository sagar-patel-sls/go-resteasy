package config

import "gitlab.com/bconway/go-resteasy/pkg/config"

const pref = "TEST_"

// Config holds settings used by test implementations.
type Config struct {
	PgURI string
}

// New instantiates a test Config, parses the environment, and returns it.
func New() *Config {
	return &Config{
		PgURI: config.String(pref+"PGURI",
			"postgres://postgres:postgres@localhost/re_test"),
	}
}
