package random

import (
	"crypto/rand"
	"encoding/hex"
)

// String returns a new random hex string n characters in length. If n < 1, a
// 5-character random string is returned.
func String(n int) string {
	if n < 1 {
		n = 5
	}

	b := make([]byte, n/2+1)
	if _, err := rand.Read(b); err != nil {
		// rand.Read should not fail. If it does, it is okay for tests to panic.
		panic(err)
	}
	return hex.EncodeToString(b)[:n]
}
