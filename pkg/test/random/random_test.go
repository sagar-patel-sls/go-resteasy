// +build !integration

package random

import (
	"fmt"
	"testing"
)

func TestString(t *testing.T) {
	t.Parallel()

	for i := -10; i < 10; i++ {
		lTest := i

		t.Run(fmt.Sprintf("Can generate %+v", lTest), func(t *testing.T) {
			t.Parallel()

			s1 := String(lTest)
			s2 := String(lTest)
			t.Logf("s1, s2: %v, %v", s1, s2)

			if lTest < 1 && (len(s1) != 5 || len(s2) != 5) {
				t.Fatalf("Expected len, actual len(s1), len(s2): 5, %v, %v",
					len(s1), len(s2))
			}
			if lTest >= 1 && (len(s1) != lTest || len(s2) != lTest) {
				t.Fatalf("Expected len, actual len(s1), len(s2): %v, %v, %v",
					lTest, len(s1), len(s2))
			}
			// Collisions on 1-character strings are common.
			if lTest >= 2 && s1 == s2 {
				t.Fatalf("s1 and s2 should not be equal: %v, %v", s1, s2)
			}
		})
	}
}
