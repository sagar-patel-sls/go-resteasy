// +build !unit

package postgres

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/bconway/go-resteasy/pkg/test/config"
)

func TestNew(t *testing.T) {
	t.Parallel()

	testConfig := config.New()

	tests := []struct {
		inp string
		err string
	}{
		// Success.
		{testConfig.PgURI, ""},
		// Database does not exist.
		{fmt.Sprintf("%s_not_exist", testConfig.PgURI), "does not exist"},
		// Wrong port.
		{"postgres://localhost:5433/db", "connect: connection refused"},
	}

	for _, test := range tests {
		lTest := test

		t.Run(fmt.Sprintf("Can connect %+v", lTest), func(t *testing.T) {
			t.Parallel()

			res, err := New(lTest.inp)
			t.Logf("res, err: %#v, %#v", res, err)
			if lTest.err == "" && res == nil {
				t.Fatalf("Expected, actual: !nil, %#v", res)
			} else if lTest.err != "" && !strings.Contains(err.Error(),
				lTest.err) {
				t.Fatalf("Expected, actual: %#v, %#v", lTest.err, err)
			}
		})
	}
}
