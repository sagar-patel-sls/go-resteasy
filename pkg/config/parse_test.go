// +build !integration

package config

import (
	"fmt"
	"math/rand"
	"os"
	"reflect"
	"strconv"
	"strings"
	"testing"
	"time"

	"gitlab.com/bconway/go-resteasy/pkg/test/random"
)

func TestString(t *testing.T) {
	t.Parallel()

	envKey := random.String(10)
	envVal := random.String(10)
	if err := os.Setenv(envKey, envVal); err != nil {
		t.Fatalf("os.Setenv should not error: %#v", err)
	}
	// Do not unset due to the mechanics of t.Parallel().

	tests := []struct {
		inpKey string
		inpDef string
		res    string
	}{
		{envKey, "", envVal},
		{random.String(10), "default", "default"},
	}

	for _, test := range tests {
		lTest := test

		t.Run(fmt.Sprintf("Can parse %+v", lTest), func(t *testing.T) {
			t.Parallel()

			res := String(lTest.inpKey, lTest.inpDef)
			t.Logf("res: %v", res)
			if res != lTest.res {
				t.Fatalf("Expected, actual: %v, %v", lTest.res, res)
			}
		})
	}
}

func TestStringSlice(t *testing.T) {
	t.Parallel()

	envKey := random.String(10)
	envVal := random.String(10) + "," + random.String(10) + "," +
		random.String(10)
	if err := os.Setenv(envKey, envVal); err != nil {
		t.Fatalf("os.Setenv should not error: %#v", err)
	}

	envKeyNoDelim := random.String(10)
	envValNoDelim := random.String(10)
	if err := os.Setenv(envKeyNoDelim, envValNoDelim); err != nil {
		t.Fatalf("os.Setenv should not error: %#v", err)
	}
	// Do not unset due to the mechanics of t.Parallel().

	tests := []struct {
		inpKey string
		inpDef []string
		res    []string
	}{
		{envKey, []string{}, strings.Split(envVal, ",")},
		{envKeyNoDelim, []string{}, []string{envValNoDelim}},
		{random.String(10), []string{"default"}, []string{"default"}},
	}

	for _, test := range tests {
		lTest := test

		t.Run(fmt.Sprintf("Can parse %+v", lTest), func(t *testing.T) {
			t.Parallel()

			res := StringSlice(lTest.inpKey, lTest.inpDef)
			t.Logf("res: %#v", res)
			if !reflect.DeepEqual(res, lTest.res) {
				t.Fatalf("Expected, actual: %#v, %#v", lTest.res, res)
			}
		})
	}
}

func TestInt(t *testing.T) {
	t.Parallel()

	envKey := random.String(10)
	envVal := rand.Intn(999)
	if err := os.Setenv(envKey, strconv.Itoa(envVal)); err != nil {
		t.Fatalf("os.Setenv should not error: %#v", err)
	}
	// Do not unset due to the mechanics of t.Parallel().

	tests := []struct {
		inpKey string
		inpDef int
		res    int
	}{
		{envKey, 0, envVal},
		{random.String(10), 99, 99},
		// Do not test conversion failure from env due to use of log.Fatalf().
	}

	for _, test := range tests {
		lTest := test

		t.Run(fmt.Sprintf("Can parse %+v", lTest), func(t *testing.T) {
			t.Parallel()

			res := Int(lTest.inpKey, lTest.inpDef)
			t.Logf("res: %v", res)
			if res != lTest.res {
				t.Fatalf("Expected, actual: %v, %v", lTest.res, res)
			}
		})
	}
}

func TestBool(t *testing.T) {
	t.Parallel()

	envKey := random.String(10)
	if err := os.Setenv(envKey, "true"); err != nil {
		t.Fatalf("os.Setenv should not error: %#v", err)
	}
	// Do not unset due to the mechanics of t.Parallel().

	tests := []struct {
		inpKey string
		inpDef bool
		res    bool
	}{
		{envKey, false, true},
		{random.String(10), true, true},
		// Do not test conversion failure from env due to use of log.Fatalf().
	}

	for _, test := range tests {
		lTest := test

		t.Run(fmt.Sprintf("Can parse %+v", lTest), func(t *testing.T) {
			t.Parallel()

			res := Bool(lTest.inpKey, lTest.inpDef)
			t.Logf("res: %v", res)
			if res != lTest.res {
				t.Fatalf("Expected, actual: %v, %v", lTest.res, res)
			}
		})
	}
}

func TestDuration(t *testing.T) {
	t.Parallel()

	envKey := random.String(10)
	envVal := rand.Intn(999)
	if err := os.Setenv(envKey, strconv.Itoa(envVal)+"s"); err != nil {
		t.Fatalf("os.Setenv should not error: %#v", err)
	}
	// Do not unset due to the mechanics of t.Parallel().

	tests := []struct {
		inpKey string
		inpDef time.Duration
		res    time.Duration
	}{
		{envKey, 0, time.Duration(envVal) * time.Second},
		{random.String(10), time.Minute, time.Minute},
		// Do not test conversion failure from env due to use of log.Fatalf().
	}

	for _, test := range tests {
		lTest := test

		t.Run(fmt.Sprintf("Can parse %+v", lTest), func(t *testing.T) {
			t.Parallel()

			res := Duration(lTest.inpKey, lTest.inpDef)
			t.Logf("res: %#v", res)
			if res != lTest.res {
				t.Fatalf("Expected, actual: %v, %v", lTest.res, res)
			}
		})
	}
}
